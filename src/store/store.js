import { createStore, applyMiddleware, compose  } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import reducers from "../reducer/reducers";
import thunk from 'redux-thunk';
const persistConfig = {
	key: "root",
	storage: storage,
	stateReconciler: autoMergeLevel2
};

const pReducer = persistReducer(persistConfig,reducers);

let composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
	pReducer,
    composeEnhancers(applyMiddleware(thunk))
);

export const persistor = persistStore(store);
