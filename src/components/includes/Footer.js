import React from "react";
import "../../App.css";
import Typography from "@material-ui/core/Typography";

function Footer(props) {
  return (
    <div className="footerPageContent">
       <Typography className="footerText">Copyright © 2021 CARGLOVE. All Rights Reserved.</Typography>
    </div>
  );
}

export default Footer;
