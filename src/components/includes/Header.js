import React, { useState, useEffect } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import SaveAnalysisModal from "./SaveAnalysisModal";
import AnalysisMenuItem from "./AnalysisMenuItem";
import SignInModal from "../user/SignInModal";
import SignUpModal from "../user/SignUpModal";
import ForgotPasswordModal from "../user/ForgotPasswordModal";
import { removeUserSession, getUser } from "../Utils/Common";
import Menu from "@material-ui/core/Menu";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import MenuItem from "@material-ui/core/MenuItem";
import "../../App.css";
import MenuIcon from "@material-ui/icons/Menu";
import { IconButton, Drawer } from "@material-ui/core";
import { connect } from "react-redux";
import { toggleSignIn } from "../../actions/actionCreators";
import VerificationSuccess from "../includes/VerificationSuccess";
import axios from "axios";

function Header(props) {
   const tokenfromhome = props.data;
  console.log("toekn from home:::" + tokenfromhome);
  const [state, setState] = useState({
    mobileView: false,
    drawerOpen: false,
  });
  const { mobileView, drawerOpen } = state;

  useEffect(() => {
    const setResponsiveness = () => {
      return window.innerWidth < 900
        ? setState((prevState) => ({ ...prevState, mobileView: true }))
        : setState((prevState) => ({ ...prevState, mobileView: false }));
    };
    setResponsiveness();
    window.addEventListener("resize", () => setResponsiveness());
  }, []);

  const signInModalOpen= props.signInModalOpen;
  const [signUpModalOpen, setSignUpModalOpen] = React.useState(false);
  const [forgotpwdModalOpen, setForgotpwdModalOpen] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorEl2, setAnchorEl2] = React.useState(null);
  const [verificationStatus, setVerficationStatus] = React.useState(false);
const [verificationResponseValue, setVerificationResponseValue] = React.useState("");
  const openSignIn = () => {
    console.log("sigin click");
    props.toggleSignIn(signInModalOpen);
  };
  
  const openSignUp = () => {
    console.log("sigun click");
    props.toggleSignIn(signInModalOpen)
    setSignUpModalOpen(true);
  };

  const openForgotpwd = () =>{
     console.log("forgot pwd click");
   props.toggleSignIn(signInModalOpen)
    setForgotpwdModalOpen(true);
  }

  const onsignInhandleClose = () => {
    console.log("inside parent close");
    //setSignInModalOpen(false);
    props.toggleSignIn(signInModalOpen);
  };

  const onsignUphandleClose = () => {
    console.log("inside parent close");
    setSignUpModalOpen(false);
  };

  const onforgotpwdhandleClose = () => {
    console.log("inside parent close");
    setForgotpwdModalOpen(false);
  };

  const handleLogout = () => {
    removeUserSession();
    setAnchorEl(null);
    window.location.reload();
  };

  const handleMenuClick = (event) => {
    setAnchorEl2(event.currentTarget);
  };

  const handleMenuClose = (event) => {
    setAnchorEl2(null);
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  
  const handleClose = () => {
    setAnchorEl(null);
  };
  useEffect(() => {
    getVerificationStatus();
    getUser();
  }, []);
  const user = getUser();
  console.log(JSON.stringify(user));

  const getVerificationStatus = () => {
    axios({
      method: "get",
      url: `${process.env.REACT_APP_API_URL}auth/verify/${tokenfromhome}`,
    })
      .then((response) => {
        if (response.data.status === "Success") {
          setVerificationResponseValue("Verification completed!");
          setVerficationStatus(true);
        } else {
          setVerficationStatus(false);
           setVerificationResponseValue("Verification InComplete");
        }
      })
      .catch((err) => {
        //handle error
        console.log("login error", err);
      });
  };
  let AuthButton;
  if (user) {
    AuthButton = (
      <div className="right-menu">
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          color="inherit"
          className="MyAnalysis-btn"
          onClick={handleMenuClick}
        >
          My Analysis
          <ArrowDropDownIcon size="small" />
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl2}
          open={Boolean(anchorEl2)}
          onClose={handleMenuClose}
          className="logoutMenu"
        >
        <AnalysisMenuItem />
        </Menu>
        <Button>
          <SaveAnalysisModal />
        </Button>

        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
          style={{ fontWeight: 500, fontSize: 15 }}
          className="user-mobile"
        >
          Welcome {user.firstName}!!
          <ArrowDropDownIcon size="small" />
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleClose}
          className="logoutMenu"
        >
          
         <MenuItem onClick={handleLogout}>Logout</MenuItem>
        </Menu>
      </div>
    );
  } else {
    AuthButton = (
      <div className="right-menu">
        <Button>
          <SaveAnalysisModal />
        </Button>
        <Button onClick={() => openSignIn()}>
          <button type="button" className="signIn-btn">
            Sign In
          </button>
        </Button>
        {signInModalOpen && (
          <SignInModal
            signInModalOpen={signInModalOpen}
            signInhandleClose={onsignInhandleClose}
            signUphandleOpen={openSignUp} 
            forgotpwdhandleOpen={openForgotpwd}
          />
        )}
        {signUpModalOpen && (
          <SignUpModal
            signUpModalOpen={signUpModalOpen}
            signUphandleClose={onsignUphandleClose}
          />
        )}
        {forgotpwdModalOpen && (
          <ForgotPasswordModal
           forgotpwdModalOpen={forgotpwdModalOpen}
           forgotpwdhandleClose={onforgotpwdhandleClose}
          />
        )}


      </div>
    );
  }

  const displayDesktop = () => {
    return (
      <Toolbar className="toolbar">
        <Typography variant="h6" className="header-title">
          <img src="./static/images/avatar/l1.png" alt="CarGlove" />
        </Typography>
        <div>{AuthButton}</div>
      </Toolbar>
    );
  };

  const displayMobile = () => {
    const handleDrawerOpen = () =>
      setState((prevState) => ({ ...prevState, drawerOpen: true }));
    const handleDrawerClose = () =>
      setState((prevState) => ({ ...prevState, drawerOpen: false }));
    return (
      <Toolbar>
        <IconButton
          {...{
            edge: "start",
            color: "inherit",
            "aria-label": "menu",
            "aria-haspopup": "true",
            onClick: handleDrawerOpen,
          }}
        >
          <MenuIcon />
        </IconButton>
        <Drawer
          {...{
            anchor: "left",
            open: drawerOpen,
            onClose: handleDrawerClose,
          }}
        >
          <div className="drawerContainer">{AuthButton}</div>
        </Drawer>
        <div>
          <Typography variant="h6" className="header-title">
            <img src="./static/images/avatar/l1.png" alt="CarGlove" />
          </Typography>
        </div>
      </Toolbar>
    );
  };

  return (
    <div>
      <AppBar position="static" className="header-section">
      {verificationStatus && <VerificationSuccess data={verificationResponseValue} signinModalOpen={signInModalOpen}/> }
        {mobileView ? displayMobile() : displayDesktop()}
      </AppBar>
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    signInModalOpen : state.signInModalOpen,
  };
};

const mapDispatchToProps = (dispatch) => ({
  toggleSignIn : (value) => dispatch(toggleSignIn(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);