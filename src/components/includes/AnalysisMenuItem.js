import React, { useState, useEffect } from "react";
import MenuItem from '@material-ui/core/MenuItem';
import { getToken } from "../Utils/Common";
import axios from "axios";
import { useHistory } from "react-router-dom";

export default function SimpleMenu() {
  const history = useHistory();
  const [data, setData] = useState([]);
  useEffect(() => {
    fetchData();
    console.log("loading--->" );
  },[]);
  
  const fetchData = () => {
    if(sessionStorage.getItem("user"))
    {
      
      let user= JSON.parse(sessionStorage.getItem("user"));
  fetch(`${process.env.REACT_APP_API_URL}data/getAnalyses?id=`+user.id, {
    method: "GET",
  })
    .then((res) => res.json())
    .then((response)=>{
      console.log("user resp"+response);
      setData(response.data.analysisResultSet);
      
      
      //setMonthlyExpediture()
    })
    .catch((error) => console.log("data error"+error));
  }
}
const handleName= (event) => {
  
    var name= event.target.innerText;
    let token = getToken();
    let user= JSON.parse(sessionStorage.getItem("user"));
    axios({
      method: "get",
      url: `${process.env.REACT_APP_API_URL}data/getAnalysis?id=`+user.id+`&name=`+name,
      
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json" 
      },
    })
      .then((response) => {
        console.log("user resp"+ response.data.data.analysisResultSet[0].name);
        let savedUrl = JSON.parse(response.data.data.analysisResultSet[0].searchState);
        let name = response.data.data.analysisResultSet[0].name;
        console.log(JSON.parse(response.data.data.analysisResultSet[0].searchState));
        history.push({
          pathname: '/',
          search: savedUrl+"&analysis="+name,
        })
        window.location.reload();
      })
      .catch((err) => {
        //handle error
        console.log("login error", err);
        
      });
  };

  return (
    <div>
      
       
          {data.length > 0 &&
          data.map(item => (
            <MenuItem onClick={handleName}>{item.name}</MenuItem>
          ))}
          {data.length < 1 &&
            <MenuItem >No Existing Analysis</MenuItem>
          }
        
    </div>
  );
}