import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import "../../App.css";
import SignInModal from "../user/SignInModal";
import SignUpModal from "../user/SignUpModal";
import ForgotPasswordModal from "../user/ForgotPasswordModal";
import { getToken } from "../Utils/Common";
import axios from "axios";
import { Alert, AlertTitle } from "@material-ui/lab";
import { connect } from "react-redux";
function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 32px !important",
  },
  textbtns: {
    textTransform: "capitalize",
    // backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    backgroundColor: "#9c1233",
    borderColor: "#9c1233",
    width: "100%",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
      backgroundColor: "#9c1233",
    },
  },
  closebtn: {
    width: 20,
    height: 20,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
    fontSize: 12,
  },
}));

function SaveAnalysisModal(props) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  const [logged, setLogged] = useState(true);
  const [signInModalOpen, setSignInModalOpen] = React.useState(false);
  const [signUpModalOpen, setSignUpModalOpen] = React.useState(false);
  const [forgotpwdModalOpen, setForgotpwdModalOpen] = React.useState(false);
  const [loading, setLoading] = useState(false);
  const [loginError, setLoginError] = useState(false);
  const [showEmptyName, setShowEmptyName] = useState(false);
  const [showInvalidName, setShowInvalidName] = useState(false);
  const [today] = useState(new Date());
  const [name, setName] = useState(today.toLocaleString());

  const handleOpen = () => {
    let user = JSON.parse(sessionStorage.getItem("user"));
    console.log("user--->" + user);

    if (user === null) {
      console.log("user is not logged in");
      setSignInModalOpen(true);
      setLogged(false);
    } else {
      setOpen(true);
      setShowEmptyName(false);
      setShowInvalidName(false);
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClick = () => {
    setOpen(false);
  };

  const openSignUp = () => {
    console.log("sigun click");
    setSignInModalOpen(false);
    setSignUpModalOpen(true);
  };

  const openForgotpwd = () => {
    console.log("forgot pwd click");
    setSignInModalOpen(false);
    setForgotpwdModalOpen(true);
  };

  const onsignInhandleClose = () => {
    console.log("inside parent signIn close");
    setSignInModalOpen(false);
  };

  const onsignUphandleClose = () => {
    console.log("inside parent close");
    setSignUpModalOpen(false);
  };

  const onforgotpwdhandleClose = () => {
    console.log("inside parent close");
    setForgotpwdModalOpen(false);
  };

  const handleSubmit = (e) => {
    setShowEmptyName(false);
    if (name === undefined || name == null || name === "") {
      console.log("empty");
      setShowEmptyName(true);
    } else {
      setShowInvalidName(false);
      setLoading(true);
      const data = window.location.search;
      var searchState = JSON.stringify(data);
      let token = getToken();
      let user = JSON.parse(sessionStorage.getItem("user"));
      axios({
        method: "get",
        url:
          `${process.env.REACT_APP_API_URL}data/getAnalysis?id=` +
          user.id +
          `&name=` +
          name,

        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }).then((response) => {
        if (response.status === 204) {
          console.log("data not");

          axios({
            method: "post",
            url: `${process.env.REACT_APP_API_URL}data/saveAnalysis`,
            data: {
              searchState: searchState,
              userId: user.id,
              name: name,
              preference: JSON.stringify(props.Preferences),
              defaultFilter: JSON.stringify(props.defaultScopes),
              optionalFilter: JSON.stringify(props.optionalFilters),
            },
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
            },
          })
            .then((response) => {
              console.log("login response: ", response);

              if (response.status === 200 || response.status === 201) {
                console.log("data saved");
                setLoading(false);
                window.location.reload();
                // browserHistory.push("/");
              }
            })
            .catch((err) => {
              //handle error

              setLoading(false);
              setLoginError(true);
              setTimeout(function () {
                window.location.reload();
              }, 1000);

              console.log("login error", err);
            });
        } else {
          console.log("already there");
          setShowInvalidName(true);
          setLoading(false);
        }
      });
    }
  };
  const body = (
    <div className="cardmodal">
      <div style={modalStyle} className={classes.paper}>
        <h2 id="simple-modal-title">Save Analysis</h2>
        <Chip label="x" className={classes.closebtn} onClick={handleClick} />
        {loading && (
          <Alert severity="info">
            <AlertTitle>Saving...</AlertTitle>
          </Alert>
        )}

        {loginError && (
          <Alert severity="warning">
            <AlertTitle>Session Expired! Logging out...</AlertTitle>
          </Alert>
        )}

        {showInvalidName && (
          <Alert severity="error">
            <AlertTitle>Analysis name already exists</AlertTitle>
          </Alert>
        )}

        {showEmptyName && (
          <Alert severity="error">
            <AlertTitle>Please Enter a name</AlertTitle>
          </Alert>
        )}
        <p id="simple-modal-description">
          <TextField
            required
            id="outlined-required"
            label="Analysis Name"
            variant="outlined"
            className="modalfields"
            onChange={(e) => setName(e.target.value)}
            value={name}
          />

          <Button
            variant="contained"
            className={classes.textbtns}
            onClick={handleSubmit}
          >
            Save Analysis
          </Button>
        </p>
      </div>
    </div>
  );

  return (
    <div>
      <button type="button" className="saveAnalysis-btn" onClick={handleOpen}>
        Save Analysis
      </button>
      {!logged ? (
        <div>
          {signInModalOpen && (
            <SignInModal
              signInModalOpen={signInModalOpen}
              signInhandleClose={onsignInhandleClose}
              signUphandleOpen={openSignUp}
              forgotpwdhandleOpen={openForgotpwd}
            />
          )}
          {signUpModalOpen && (
            <SignUpModal
              signUpModalOpen={signUpModalOpen}
              signUphandleClose={onsignUphandleClose}
            />
          )}
          {forgotpwdModalOpen && (
            <ForgotPasswordModal
              forgotpwdModalOpen={forgotpwdModalOpen}
              forgotpwdhandleClose={onforgotpwdhandleClose}
            />
          )}
        </div>
      ) : null}
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}

const mapStateToProps = (state) => ({
  Preferences: state.defaultPreferences,
  defaultScopes: state.defaultfilters,
  optionalFilters: state.optionalFilters,
});
export default connect(mapStateToProps, null)(SaveAnalysisModal);
