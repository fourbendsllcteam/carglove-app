import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    top: -5,
    color: "#ffffff",
    right: -5,
    padding: 0,
    position: "absolute",
    background: "#dc3545",
    width: 15,
    height: 15,
    "&:hover": {
      background: "#dc3545",
    },
  },
  title: {
    margin: 0,
    padding: theme.spacing(2),
    position: "relative",
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.title} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon fontSize="small" />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function Dialogmodal() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [fullWidth, setFullWidth] = React.useState(true);
  const [maxWidth, setMaxWidth] = React.useState("sm");

  return (
    <div>
      <Button
        variant="contained"
        color="primary"
        size="small"
        onClick={handleClickOpen}
        style={{
          marginRight: 10,
          textTransform: "capitalize",
          fontSize: 14,
          marginTop: 20,
        }}
      >
        Complete
      </Button>

      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        fullWidth={fullWidth}
        maxWidth={maxWidth}
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
          style={{ textAlign: "center", fontSize: 18, color: "#4f4f4f" }}
        >
          Car Detail
        </DialogTitle>
        <DialogContent>
          <TextField
            required
            id="outlined-required"
            label="First Name"
            variant="outlined"
            className="modalfields"
            type="String"
          />

          <TextField
            required
            id="outlined-required"
            label="Last Name"
            variant="outlined"
            className="modalfields"
            type="String"
          />

          <TextField
            required
            id="outlined-required"
            label="Title"
            variant="outlined"
            className="modalfields"
            type="String"
          />

          <TextField
            required
            id="outlined-required"
            label="User Name"
            variant="outlined"
            className="modalfields"
            type="String"
          />

          <TextField
            required
            id="outlined-required"
            label="E-mail"
            variant="outlined"
            className="modalfields"
            type="email"
          />

          <TextField
            required
            id="outlined-required"
            label="Password"
            variant="outlined"
            className="modalfields"
            type="password"
          />

          <TextField
            required
            id="outlined-required"
            label="Mobile no"
            variant="outlined"
            className="modalfields"
            type="tel"
          />

          <TextField
            required
            id="outlined-required"
            label="Landline no"
            variant="outlined"
            className="modalfields"
            type="tel"
          />

          <TextField
            required
            id="outlined-required"
            label="Address"
            variant="outlined"
            className="modalfields"
            multiline
            rows={2}
            type="String"
          />
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} style={{ color: "#9c1233" }}>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
