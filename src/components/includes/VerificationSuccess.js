import React, { useEffect} from "react";
import Backdrop from "@material-ui/core/Backdrop";
import { makeStyles } from "@material-ui/core/styles";
import { Alert, AlertTitle } from "@material-ui/lab";
import { toggleSignIn } from "../../actions/actionCreators";
import { connect } from "react-redux";

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

function VerificationSuccess(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
   const [counter, setCounter] = React.useState(0);
  //const [signInModalOpen, setSignInModalOpen] = React.useState(true);
  const signInModalOpen = props.signinModalOpen;
  console.log("SignInmodal open in verification success" + signInModalOpen);
  const value = props.data;
 
  const handleClose = () => {

     if (open) {
      setCounter(counter + 1);
    }
      setOpen(false);
  };

  useEffect(() => {
    if (counter === 1) {
      props.toggleSignIn(signInModalOpen);
    }
    //props.toggleSignIn(signInModalOpen);
  }, [counter]);

  return (
    <div>
      <Backdrop className={classes.backdrop} open={open} onClick={()=>handleClose()}>
        <Alert onClose={handleClose} severity="info">
          <AlertTitle>{value}</AlertTitle>
        </Alert>
      </Backdrop>
      <div></div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => ({
  toggleSignIn: (value) => dispatch(toggleSignIn(value)),
});

export default connect("", mapDispatchToProps)(VerificationSuccess);
