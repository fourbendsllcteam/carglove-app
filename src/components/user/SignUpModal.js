import React, { useState, useReducer } from "react";
import axios from "axios";
import ReCAPTCHA from "react-google-recaptcha";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import "../../App.css";
import { Scrollbars } from "react-custom-scrollbars";
import { Alert, AlertTitle } from "@material-ui/lab";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const passwordRegExp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;

const SignupSchema = yup.object().shape({
  firstName: yup.string().required(),
  lastName: yup.string().required(),
  title: yup.string().required(),
  username: yup.string().required(),
  email: yup.string().email().required(),
  password: yup
    .string()
    .required("No password provided.")
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(
      passwordRegExp,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character."
    ),
  confirmPassword: yup
    .string()
    .required()
    .oneOf([yup.ref("password"), null], "Passwords must match"),
  mobile: yup
    .string()
    .matches(new RegExp("[0-9]{10}"), "Phone number is not valid"),
  landline: yup.string().matches(
    phoneRegExp,
    {
      excludeEmptyString: true,
    },
    "Landline number is not valid"
  ),
  address: yup.string().required(),
});

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 32px !important",
  },
  textbtns: {
    textTransform: "capitalize",
    backgroundColor: "#9c1233",
    //backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
      backgroundColor: "#9c1233",
    },
  },
  closebtn: {
    width: 20,
    height: 20,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
    fontSize: 12,
  },
  linktxt1: {
    fontSize: 14,
    color: "#ea001e",
    "&:hover": {
      color: "#ea001e",
    },
  },
}));

const formReducer = (state, event) => {
  if (event.reset) {
    return {
      username: "",
      password: "",
      email: "",
      firstName: "",
      lastName: "",
      title: "",
      mobile: "",
      landline: "",
      address: "",
    };
  }
  return {
    ...state,
    [event.name]: event.value,
  };
};

export default function SignUpModal(props) {
  const classes = useStyles();

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(SignupSchema),
  });

  //user variables
  const [successful, setSuccessful] = useState(false);
  const [captchaResponse, setCaptchaResponse] = useState("");
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [submitting, setSubmitting] = useState(false);
  const [sentVerificationMail, setSentVerificationMail] = useState(false);
  const [formData, setFormData] = useReducer(formReducer, {});
  const [errormsg, setErrormsg] = useState(false);

  const onChangeCaptcha = (value) => {
    setCaptchaResponse(value);
  };

  //onform submit
  const handleRegister = () => {
    setSubmitting(true);
    console.log("inside handleRegister");
    setErrormsg("");
    setSuccessful(false);

    axios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}auth/register`,
      data: {
        username: formData.username,
        password: formData.password,
        email: formData.email,
        firstName: formData.firstName,
        lastName: formData.lastName,
        title: formData.title,
        mobile: formData.mobile,
        landline: formData.landline,
        address: formData.address,
        captchaResponse: captchaResponse,
      },
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => {
        console.log("register response: ", response);
        if (response.status === 201) {
          setSubmitting(false);
          setSentVerificationMail(true);

          setTimeout(() => {
            //props.setSignUpModalOpen(false);
            setSentVerificationMail(false);
          }, 4000);
          setSuccessful(true);
          props.signUphandleClose();
        }
      })
      .catch((err) => {
        //handle error
        if (err.response) {
          // Request made and server responded
          setErrormsg(err.response.data.message);
        } else if (err.request) {
          // The request was made but no response was received
          console.log(err.request);
          setErrormsg("Registeration failed, Please try again");
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", err.message);
          setErrormsg("Network Error : Please check the API services");
        }
        setSubmitting(false);
      });
  };

  const handleChange = (event) => {
    console.log(
      "inside handleChange",
      event.target.name + " val: " + event.target.value
    );
    setFormData({
      name: event.target.name,
      value: event.target.value,
    });
  };

  console.log(errors);
  return (
    <div>
      <Modal
        open={props.signUpModalOpen}
        onClose={props.signUphandleClose}
        aria-labelledby="signup-modal-title"
        aria-describedby="signup-modal-description"
      >
        <form onSubmit={handleSubmit(handleRegister)}>
          <div className="cardmodal">
            <div style={modalStyle} className={classes.paper}>
              <h2 id="signup-modal-title">Sign Up</h2>
              <Chip
                label="x"
                className={classes.closebtn}
                onClick={props.signUphandleClose}
              />
              {!successful && (
                <Scrollbars style={{ height: 450 }}>
                  <p id="signup-modal-description" class="signup">
                    <TextField
                      id="outlined-required"
                      label="First Name *"
                      name="firstName"
                      variant="outlined"
                      className="modalfields"
                      type="String"
                      style={{ marginTop: 15 }}
                      onChange={handleChange}
                      inputRef={register}
                      error={errors.firstName}
                    />
                    <TextField
                      id="outlined-required"
                      label="Last Name *"
                      name="lastName"
                      variant="outlined"
                      className="modalfields"
                      type="String"
                      onChange={handleChange}
                      inputRef={register}
                      error={errors.lastName}
                    />
                    
                    <FormControl
                      variant="outlined"
                      className="modalfields"
                      error={errors.title}
                    >
                      <InputLabel htmlFor="outlined-age-native-simple">
                        Title *
                      </InputLabel>
                      <Select
                        native
                        value={formData.title}
                        onChange={handleChange}
                        label="Title *"
                        inputProps={{
                          name: "title",
                          id: "outlined-age-native-simple",
                          value :formData.title
                        }}
                        error={errors.title}
                        inputRef={register}
                      >
                        <option aria-label="None" value="" />
                        <option value={"Mr."}>Mr.</option>
                        <option value={"Mrs."}>Mrs.</option>
                        <option value={"Ms."}>Ms.</option>
                      </Select>
                    </FormControl>
                    <TextField
                      id="outlined-required"
                      label="User Name *"
                      name="username"
                      variant="outlined"
                      className="modalfields"
                      type="String"
                      onChange={handleChange}
                      inputRef={register}
                      error={errors.username}
                    />

                    <TextField
                      id="outlined-required"
                      label="Email *"
                      name="email"
                      variant="outlined"
                      className="modalfields"
                      type="String"
                      onChange={handleChange}
                      inputRef={register}
                      error={errors.email}
                      helperText={
                        errors.email && "Please enter valid email address"
                      }
                    />

                    <TextField
                      id="outlined-required"
                      label="Password *"
                      name="password"
                      variant="outlined"
                      className="modalfields"
                      type="password"
                      onChange={handleChange}
                      inputRef={register}
                      error={errors.password}
                      helperText={
                        errors.password &&
                        "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
                      }
                    />

                    <TextField
                      id="outlined-required"
                      label="Confirm Password *"
                      name="confirmPassword"
                      variant="outlined"
                      className="modalfields"
                      type="password"
                      inputRef={register}
                      error={errors.confirmPassword}
                      helperText={
                        errors.confirmPassword && "Password is not matching"
                      }
                    />

                    <TextField
                      id="outlined-required"
                      label="Mobile Number *"
                      name="mobile"
                      variant="outlined"
                      className="modalfields"
                      type="N"
                      onChange={handleChange}
                      inputRef={register}
                      error={errors.mobile}
                    />

                    <TextField
                      id="outlined"
                      label="Landline Number"
                      name="landline"
                      variant="outlined"
                      className="modalfields"
                      type="String"
                      onChange={handleChange}
                      inputRef={register}
                      error={errors.landline}
                    />

                    <TextField
                      id="outlined-required"
                      label="Address *"
                      name="address"
                      variant="outlined"
                      className="modalfields"
                      multiline
                      rows={2}
                      type="String"
                      onChange={handleChange}
                      inputRef={register}
                      error={errors.address}
                    />
                    <ReCAPTCHA
                      sitekey="6Lc_noUaAAAAAFjJEOE8R8MLXnuDdncNkEDSUBX3"
                      onChange={onChangeCaptcha}
                      size="normal"
                    />
                  </p>
                </Scrollbars>
              )}
              {submitting && (
                <Alert severity="info">
                  <AlertTitle>Registration processing...</AlertTitle>
                </Alert>
              )}
              {sentVerificationMail && (
                <Alert severity="success">
                  <AlertTitle>
                    Verification email sent. Please check.
                  </AlertTitle>
                </Alert>
              )}
              {errormsg && (
                <Alert severity={successful ? "success" : "error"}>
                  <AlertTitle>{errormsg}</AlertTitle>
                </Alert>
              )}
              <Button
                variant="contained"
                className={classes.textbtns}
                id="action"
                type="submit"
              >
                Sign Up{" "}
              </Button>
            </div>
          </div>
        </form>
      </Modal>
    </div>
  );
}
