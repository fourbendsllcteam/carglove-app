import axios from "axios";
import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Alert, AlertTitle } from "@material-ui/lab";

const passwordRegExp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
const ChangePwdSchema = yup.object().shape({
  password: yup
    .string()
    .required("No password provided.")
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(
      passwordRegExp,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character."
    ),
  retypePassword: yup
    .string()
    .required()
    .oneOf([yup.ref("password"), null], "Passwords must match"),
});
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    borderRight: 0,
    marginBottom: 10,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },
}));

const formReducer = (state, event) => {
  if (event.reset) {
    return {
      password: "",
    };
  }
  return {
    ...state,
    [event.name]: event.value,
  };
};

export default function ChangePassword(props) {
  const classes = useStyles();
  const history = useHistory();
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(ChangePwdSchema),
  });
  const [submitting, setSubmitting] = React.useState(false);
  const [formData, setFormData] = React.useReducer(formReducer, {});
  const [errormsg, setErrormsg] = React.useState(false);
  const [successful, setSuccessful] = React.useState(false);

  
  const [token] = React.useState(props.location.search);

  console.log("token--->" + token);
 

  const handlePasswordChange = () => {
    console.log("inside handleSubmit");
    setSubmitting(true);
    setErrormsg("");
    setSuccessful(false);
        axios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}auth/reset-password${token}`,
      data: { password: formData.password },
    })
      .then((response) => {
        console.log("login response: ", response);
        if (response.status === 200) {
          setSubmitting(false);
           setSuccessful(true);
          history.push("/");
        }
      })
      .catch((err) => {
        //handle error
        console.log("login error", err);
        console.log("login error", err);
        //handle error
        if (err.response) {
          // Request made and server responded
          setErrormsg(err.response.data.message);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", err.message);
          setErrormsg("Network Error : Please check the API services");
        }
        setSubmitting(false);
      });
  };

  const handleChange = (event) => {
    setFormData({
      name: event.target.name,
      value: event.target.value,
    });
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Reset Password
        </Typography>
         {!successful && (
        <form onSubmit={handleSubmit(handlePasswordChange)}>
          <fieldset disabled={submitting}>
            <TextField
              id="outlined-required"
              label="Password"
              name="password"
              variant="outlined"
              className="modalfields"
              type="password"
              onChange={handleChange}
              inputRef={register}
              error={errors.password}
              helperText={
                errors.password &&
                "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
              }
            />

            <TextField
              id="outlined-required"
              label="Retype Password"
              name="retypePassword"
              variant="outlined"
              className="modalfields"
              type="password"
              inputRef={register}
              error={errors.retypePassword}
              helperText={errors.retypePassword && "Password is not matching"}
            />
            {submitting && (
              <Alert severity="info">
                <AlertTitle>Changing Password...</AlertTitle>
              </Alert>
            )}
          </fieldset>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            className={classes.submit}
            disabled={submitting}
          >
            Submit
          </Button>
          {errormsg && (
              <div className="form-group">
                <div
                  className={
                    successful ? "alert alert-success" : "alert alert-danger"
                  }
                  role="alert"
                >
                  {errormsg}
                </div>
              </div>
            )}
        </form>
          )}
      </div>
    </Container>
  );
}
