import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import axios from "axios";
import validator from "validator";
import { Alert, AlertTitle } from "@material-ui/lab";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  linktxt: {
    fontSize: 14,
    float: "right",
    color: "#ea001e",
    "&:hover": {
      color: "#ea001e",
    },
  },
  textbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    borderRight: 0,
    marginBottom: 10,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },
  closebtn: {
    width: 20,
    height: 20,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
    fontSize: 12,
  },
}));

export default function ForgotPasswordModal(props) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [email, setEmail] = React.useState();
  const [emailError, setEmailError] = React.useState("");
  const [disable, setDisable] = React.useState(true);
  const [sentVerificationMail, setSentVerificationMail] = React.useState(false);

  const handleChange = (event) => {
    setEmail(event.target.value);
    setDisable(false);
  };

  const handleSubmit = () => {
    if (validator.isEmail(email)) {
      setEmailError("");
    } else {
      setEmailError("Enter valid Email!");
    }
    console.log("inside handleSubmit");
    axios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}auth/forgot-password`,
      data: { email: email },
    })
      .then((response) => {
        if (response.status === 204) {
          setEmail("");
          setSentVerificationMail(true);
          console.log("login response: ", response);
          setTimeout(() => {
            props.forgotpwdhandleClose();
          }, 5000);
          
        }
      })
      .catch((err) => {
        //handle error
        console.log("login error", err);
        //handle error
        if (err.response) {
          // Request made and server responded
          setEmailError(err.response.data.message);
        }  else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", err.message);
          setEmailError("Network Error : Please check the API services");
        }
      });
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title">Forgot Your Password?</h2>
      <Chip label="x" className={classes.closebtn} onClick={props.forgotpwdhandleClose}/>

      <p id="singnIn-modal-description">
        <TextField
          id="outlined-required"
          name="email"
          variant="outlined"
          className="modalfields"
          label="Registered Email"
          onChange={handleChange}
          value={email}
          helperText={emailError}
          error={emailError}
        />
        {sentVerificationMail && (
          <Alert severity="success">
            <AlertTitle>Password reset email sent. Please check.</AlertTitle>
          </Alert>
        )}
        <Button
          type="submit"
          variant="contained"
          className={classes.textbtns}
          onClick={handleSubmit}
          disabled={disable}
        >
          Submit
        </Button>
      </p>
    </div>
  );

  return (
    <div>
      <Modal
        open={props.forgotpwdModalOpen}
        onClose={props.forgotpwdhandleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
