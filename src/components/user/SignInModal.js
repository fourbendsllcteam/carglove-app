import axios from "axios";
import React, { useState, useReducer } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import "../../App.css";
import { Alert, AlertTitle } from "@material-ui/lab";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import { useHistory, useLocation } from "react-router-dom";
import Checkbox from "@material-ui/core/Checkbox";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { setUserSession } from "../Utils/Common";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

const SignInSchema = yup.object().shape({
  username: yup.string().required(),
  password: yup.string().required(),
});

//styles
function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 32px !important",
  },
  textbtns: {
    textTransform: "capitalize",
    backgroundColor: "#9c1233",
   // backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    borderRight: 0,
    marginBottom: 10,
    color: "#fff",
    "&:hover": {
      color: "#fff",
      backgroundColor: "#9c1233",
    },
  },
  closebtn: {
    width: 20,
    height: 20,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
    fontSize: 12,
  },
  linktxt: {
    fontSize: 14,
    float: "right",
    color: "#ea001e",
    "&:hover": {
      color: "#ea001e",
    },
  },

  linktxt1: {
    fontSize: 14,
    color: "#ea001e",
    "&:hover": {
      color: "#ea001e",
    },
  },

  remember: {},
}));

const formReducer = (state, event) => {
  if (event.reset) {
    return {
      username: "",
      password: "",
    };
  }
  return {
    ...state,
    [event.name]: event.value,
  };
};

export default function SignInModal(props) {
  //styles
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();
  const [modalStyle] = React.useState(getModalStyle);

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(SignInSchema),
  });

  //modal realted
  const [submitting, setSubmitting] = useState(false);
  const [showInvalidCreds, setShowInvalidCreds] = useState(false);
  const [showNetworkFailure, setShowNetworkFailure] = useState(false);
  const [formData, setFormData] = useReducer(formReducer, {});

  //user attributes
  const [showPassword, setShowPassword] = useState(false);
  const [rememberMe, setRememberMe] = useState(false);

  const [firstName, setFirstName] = useState();
  //behaviour flags
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const handleChange = (event) => {
    setFormData({
      name: event.target.name,
      value: event.target.value,
    });
  };

  const handleLogin = () => {
    console.log("inside handleSubmit");
    setSubmitting(true);

    axios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}auth/login`,
      data: { username: formData.username, password: formData.password },
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => {
        console.log("login response: ", response);
        if (response.status === 200) {
          setSubmitting(false);
          setUserSession(response.data.data.tokens, response.data.data.user);
          setIsLoggedIn(true);
          setFirstName(response.data.data.user.firstName);
          props.signInhandleClose();
          console.log(location.search);
          /*history.replace({
            pathname: '/home',
            search: location.search,
          })*/
          window.location.reload();
        }
      })
      .catch((err) => {
        //handle error
        console.log("login error", err);
        if (err.toString().includes("401")) {
          setSubmitting(false);
          setShowNetworkFailure(false);
          setShowInvalidCreds(true);
        } else {
          setSubmitting(false);
          setShowInvalidCreds(false);
          setShowNetworkFailure(true);
        }
      });
  };

  return (
    <div>
      <Modal
        open={props.signInModalOpen}
        onClose={props.signInhandleClose}
        aria-labelledby="singnIn-modal-title"
        aria-describedby="singnIn-modal-description"
      >
        <div className="cardmodal">
          <div style={modalStyle} className={classes.paper}>
            <h2 id="singnIn-modal-title">Sign In</h2>
            <Chip
              label="x"
              className={classes.closebtn}
              onClick={props.signInhandleClose}
            />
            {submitting && (
              <Alert severity="info">
                <AlertTitle>Authenticating...</AlertTitle>
              </Alert>
            )}

            {showInvalidCreds && (
              <Alert severity="error">
                <AlertTitle>
                  Invalid login credentials, Please try again
                </AlertTitle>
              </Alert>
            )}

            {showNetworkFailure && (
              <Alert severity="warning">
                <AlertTitle>
                  Network Error : Please check the API services
                </AlertTitle>
              </Alert>
            )}
            <p id="singnIn-modal-description">
              <form onSubmit={handleSubmit(handleLogin)}>
                <fieldset disabled={submitting}>
                  <TextField
                    id="outlined-required"
                    name="username"
                    label="User Name *"
                    variant="outlined"
                    className="modalfields"
                    onChange={handleChange}
                    inputRef={register}
                    error={errors.username}
                  />
                  <TextField
                    label="Password *"
                    variant="outlined"
                    className="modalfields"
                    name="password"
                    type={showPassword ? "text" : "password"} // <-- This is where the magic happens
                    onChange={handleChange}
                    inputRef={register}
                    error={errors.password}
                    InputProps={{
                      // <-- This is where the toggle button is added.
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={(e) => setShowPassword(!showPassword)}
                          >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />

                  <FormControlLabel
                    control={<Checkbox value="remember" />}
                    label="Remember me"
                    className={classes.remember}
                    onClick={(e) => setRememberMe(!rememberMe)}
                  />
                </fieldset>
                <Button
                  type="submit"
                  variant="contained"
                  className={classes.textbtns}
                  disabled={submitting}
                >
                  Sign In
                </Button>
              </form>

              <Grid container>
                <Grid item xs>
                  <Link
                    component="button"
                    className={classes.linktxt1}
                    onClick={() => props.signUphandleOpen()}
                  >
                    {"Don't have an account? Sign Up"}
                  </Link>
                  <Link
                    component="button"
                    className={classes.linktxt}
                    onClick={() => props.forgotpwdhandleOpen()}
                  >
                    Forgot password
                  </Link>
                </Grid>
              </Grid>
            </p>
          </div>
        </div>
      </Modal>
    </div>
  );
}
