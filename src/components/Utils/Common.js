import axios from "axios";
const jwt = require("jsonwebtoken");

// return the user data from the session storage
export const getUser = () => {
  const userStr = sessionStorage.getItem("user");
  if (userStr) return JSON.parse(userStr);
  else return null;
};

// return the token from the session storage
export const getToken = () => {
  console.log("gettingToken...");

  let accessToken = sessionStorage.getItem("accessToken");
  let refreshTokenValue = sessionStorage.getItem("refreshToken");

  if (accessToken !== null) {
    let decodedToken = jwt.decode(accessToken, { complete: true });

    const tokenExpired = isAccessTokenExpired(decodedToken);
    if (tokenExpired) {
      refreshToken(refreshTokenValue);
      accessToken = sessionStorage.getItem("accessToken");
      refreshTokenValue = sessionStorage.getItem("refreshToken");
    }
  }

  return accessToken || null;
};

const refreshToken = (refreshTokenValue) => {
  console.log("refreshing tokens...");
  axios({
    method: "post",
    url:  `${process.env.REACT_APP_API_URL}auth/refresh-tokens`,
    data: { refreshToken: refreshTokenValue },
    headers: { "Content-Type": "application/json" },
  })
    .then((response) => {
      if (response.status === 200) {
        // console.log("token refreshed, " + JSON.stringify(response));
        // console.log("token refreshed, " + response);
        sessionStorage.setItem(
          "accessToken",
          response.data.data.tokens.access.token
        );
        sessionStorage.setItem(
          "refreshToken",
          response.data.data.tokens.refresh.token
        );
      } else {
        sessionStorage.clear();
        console.log("session cleared");
      }
    })
    .catch((err) => {
      console.log(err);
      sessionStorage.clear();
      console.log("exception occured, session cleared");
    });
};

const isAccessTokenExpired = (decodedToken) => {
  let currendTimestamp = new Date();
  if (decodedToken.payload.exp * 1000 < currendTimestamp.getTime()) {
    console.log("access token expired, refreshing tokens...");
    return 1;
  } else return 0;
};

// remove the token and user from the session storage
export const removeUserSession = () => {
  sessionStorage.removeItem("accessToken");
  sessionStorage.removeItem("refreshToken");
  sessionStorage.removeItem("user");
};

// set the token and user from the session storage
export const setUserSession = (token, user) => {
  sessionStorage.setItem("accessToken", token.access.token);
  sessionStorage.setItem("refreshToken", token.refresh.token);
  sessionStorage.setItem("user", JSON.stringify(user));
};

export const setFinancialSession = (data) => {
  sessionStorage.setItem("financialData", JSON.stringify(data));
  
};
