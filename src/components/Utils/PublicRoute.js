import React from "react";
import { Route } from "react-router-dom";
import { getToken } from "./Common";

// handle the public routes
function PublicRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) =>
        !getToken() ? (
          <Component {...props} />
        ) : (
          <Component {...props} />
        )
      }
    />
  );
}

export default PublicRoute;
