import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import {
	ReactiveComponent,
} from '@appbaseio/reactivesearch';
const useStyles = makeStyles({
  root: {
    height: 150,
  },
});

function valuetext(value) {
  return `${value}°C`;
}

const marks = [
  {
    value: 0,
    label: '0°C',
  },
  {
    value: 20,
    label: '20°C',
  },
  {
    value: 37,
    label: '37°C',
  },
  {
    value: 100,
    label: '100°C',
  },
];

function SliderWrapper(props){
  const [value, setValue] = React.useState(50);
  const handleChange = (event, newValue) => {
    setValue(newValue);

    const query = {
      query: {
        range: {
          "model_data.noiseLevel": {
            "lte": newValue
          }
        }
      }
    }

    props.setQuery({
        query,
        value: newValue,
    });

  };

    return (
      <Slider
        orientation="vertical"
        getAriaValueText={valuetext}
        value={value}
        aria-labelledby="vertical-slider"
        onChangeCommitted={handleChange}
      />
    );
}
export default function PreferenceSlider() {
  const classes = useStyles();

  return (
    <ReactiveComponent
      componentId="NoiseFilterComponent" // a unique id we will refer to later
      dataField="model_data.noiseLevel"  
      defaultQuery={() => ({
        "query": {
          "range": {
            "model_data.noiseLevel": {
              "lte": 50
            }
          }
        }
      })}
        react={{
          and: ["result"],
      }}
      render={({ aggregations, setQuery }) => (
        <div className={classes.root}>
          <SliderWrapper aggregations={aggregations} setQuery={setQuery} />
        </div>
          
      )}
  />
  );
}
