import React, { forwardRef} from "react";
import "./styles.css";
import Slider from "react-rangeslider";
import Chip from "@material-ui/core/Chip";
import Tooltip from "@material-ui/core/Tooltip";
import {  useDispatch  } from "react-redux";
import { remove_default_preferences } from '../../actions/actionCreators';
function SliderWrapper(props){
  const [value, setValue] = React.useState(50);
  const handleChange = (newValue, newfield) => {
    setValue(newValue);
  };

    return (
      <>
      <Chip label={value} className="badgevalue" />
        <Slider
          value={value}
          orientation="vertical"
          onChange={(e) => handleChange(e,props.field)} 
        />
      </>
    );
}

export default forwardRef(({ data, classValue }, ref) => {
  const dispatch = useDispatch()

 const removePreference = (data) => {
    dispatch(remove_default_preferences(data))
 }
 
  return (
    <div className={`item ${classValue}`} ref={ref}>
      <button className="closebutton" name="removeTask" onClick={(e) => removePreference(data)}>
        x
      </button>
      <Tooltip title={<h6 style={{ color: "white", fontSize: "12px", fontWeight: "normal", fontFamily: "sans-serif" }}>{data.description}</h6>} arrow placement="top">
        <span className="rangeSlider_values">{data.label}</span>
      </Tooltip>
        <SliderWrapper  classValue={classValue} ref={ref} data={data}  />

    </div>
  );
});