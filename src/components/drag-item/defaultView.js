import React from "react";
import "./styles.css";
import Slider from "react-rangeslider";
import Chip from "@material-ui/core/Chip";
import Tooltip from "@material-ui/core/Tooltip";
import {  useDispatch  } from "react-redux";
import { remove_default_preferences, updatePreferences } from '../../actions/actionCreators';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
function SliderWrapper(props){
  const { data } = props;
  const [value, setValue] = React.useState(data.defaultValue);
  const dispatch = useDispatch()

  const handleChange = (newValue, ID, dValue) => {
    setValue(newValue);
    dispatch(updatePreferences(newValue, ID))
    console.log(ID);
  };

    return (
      <>
      <Chip label={props.data.defaultValue} className="badgevalue" />
        <Slider
          value={props.data.defaultValue}
          orientation="vertical"
          onChange={(e) => handleChange(e,data.id,data.defaultValue)} 
        />
      </>
    );
}

const DefaultView = (props) => {

 const { data,allPreferences  } = props;
 const [openToast, setToastOpen] = React.useState(false);
 const dispatch = useDispatch()

 const filteredPreferences = Object.values(allPreferences).filter(
  (pref) => pref.visibility === "default"
);

 const removePreference = (data) => {
    console.log(filteredPreferences.length);
    if(filteredPreferences.length > 1){
      dispatch(remove_default_preferences(data))
    }else{
      setToastOpen(true);
    }
 }
 
 const handleClose = (event, reason) => {
  if (reason === 'clickaway') {
    return;
  }

  setToastOpen(false);
};

  return (
    <div className={`item`}>
      <button className="closebutton" name="removeTask" onClick={(e) => removePreference(data)}>
        x
      </button>
      <Tooltip title={<h6 style={{ color: "white", fontSize: "12px", fontWeight: "normal", fontFamily: "sans-serif" }}>{data.description}</h6>} arrow placement="top">
        <span className="rangeSlider_values">{data.label}</span>
      </Tooltip>
      <SliderWrapper   data={data} field={data.field} />

      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        open={openToast}
        autoHideDuration={6000}
        onClose={handleClose}
        message="There must be at least one preferences by default"
        action={
          <React.Fragment>
            <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />

    </div>
  );
};
export default DefaultView;