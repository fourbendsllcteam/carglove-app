import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import "../../App.css";
import Grid from "@material-ui/core/Grid";
import { Scrollbars } from "react-custom-scrollbars";
import InputAdornment from "@material-ui/core/InputAdornment";
import { getToken } from "../Utils/Common";
import axios from "axios";
import { Alert, AlertTitle } from "@material-ui/lab";
import { setFinancialSession } from "../Utils/Common";
import { connect } from "react-redux";
import {
  setFinancial,
  setElecUnits,
  setFuelCons,
  setMonthlyExpenditure,
  setPetrol,
  setDiesel,
  setTravellingMnths,
  setInsurance,
  isCustomFinancial,
  setMonthlyInstallment,
  setMaxPrice,
  setBalloonPercent,
  setBalloonRand,
  setDepositPercent,
  setDepositRand,
  setInterest,
  setPeriodMnths,
  setFuelSupply
} from "../../actions/actionCreators";
import currencyFormatter from "currency-formatter";
import Radio from '@material-ui/core/Radio';
import Typography from "@material-ui/core/Typography";


function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 700,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 32px !important",
  },
  textbtns: {
    textTransform: "capitalize",
    backgroundColor: "#9c1233",
    // backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: 90,
    marginRight: 15,
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
      backgroundColor: "#9c1233",
    },
  },
  closebtn: {
    width: 20,
    height: 20,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
    fontSize: 12,
  },
}));

const FinancialsModal = (props) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);

  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  const [disabled] = React.useState(true);
  const [financialValues, setFinancialValues] = React.useState([]);
  const [periodInYrsValue, setPeriodInYrsValue] = React.useState();

  const [travellingpyValue, setTravellingpyValue] = React.useState();

  const [depositInRandValue, setDepositInRandValue] = React.useState();

  const [balloonInRandValue, setBalloonInRandValue] = React.useState();

  const [successful, setSuccessful] = React.useState();
  const [financialData, setFinancialData] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState('petrol');
  const [showSavedMsg, setShowSavedMsg] = useState(false);

  const handleRadioChange = (event) => {
    setSelectedValue(event.target.value);
    /*if (event.target.value === 'electric') {
      props.setFuelSupply(props.elecUnits);
    }
    else if (event.target.value === 'diesel') {
      props.setFuelSupply(props.diesel);
    }
    else {
      props.setFuelSupply(props.petrol);
    }*/
    props.setFuelSupply(event.target.value);
    props.setMonthlyExpenditure();
  };

  useEffect(() => {
    fetchData();
    setData();
    console.log("loading--->" + loading);
  }, []);

  useEffect(() => {
    console.log("Do something after counter has changed" + depositInRandValue);
    calculation();
  }, [depositInRandValue, balloonInRandValue]);

  const fetchData = () => {
    setLoading(true);
    props.isCustomFinancial(false);
    let fData;
    console.log("default data");
    fetch(`${process.env.REACT_APP_API_URL}data/financial`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((response) => {
        console.log(response.data);
        fData = response.data.financialResultSet;
        console.log("balloon in default --> " + fData);
        setFinancialValues(response.data.financialResultSet[0]);
        props.setDiesel(response.data.financialResultSet[0].diesel50ppm);
        props.setPetrol(response.data.financialResultSet[0].petrol95);
        setLoading(false);
        props.setInsurance(response.data.financialResultSet[0].insurance);
        setPeriodInYrsValue(response.data.financialResultSet[0].periodInYrs);

        props.setTravellingMnths(
          response.data.financialResultSet[0].travellingpm
        );
        setTravellingpyValue(response.data.financialResultSet[0].travellingpy);
        props.setPeriodMnths(response.data.financialResultSet[0].periodInMnths);

        props.setDepositPercent(
          Number(response.data.financialResultSet[0].depositInPercent)
        );
        props.setDepositRand(response.data.financialResultSet[0].depositInRand);
        //setDepositInRandValue(response.data.financialResultSet[0].depositInRand);
        props.setBalloonPercent(
          Number(response.data.financialResultSet[0].balloonInPercent)
        );
        props.setBalloonRand(
          Number(response.data.financialResultSet[0].balloonInRand)
        );
        props.setInterest(response.data.financialResultSet[0].interestRate);

        props.setFuelCons(response.data.financialResultSet[0].fuelCons);
        props.setMonthlyExpenditure();
        props.setElecUnits(response.data.financialResultSet[0].electUnits);
        //props.setFuelSupply(response.data.financialResultSet[0].petrol95);
        setFinancialData(false);
        if (sessionStorage.getItem("user")) {
          let user = JSON.parse(sessionStorage.getItem("user"));
          console.log("user data");
          fetch(
            `${process.env.REACT_APP_API_URL}data/userfinancial?id=` + user.id,
            {
              method: "GET",
            }
          )
            .then((res) => res.json())
            .then((response) => {
              console.log("user resp" + response);

              fData = response.data.financialResultSet;
              console.log("user resp" + fData);
              setFinancialData(true);
              props.isCustomFinancial(true);
              console.log("balloon in % --> " + fData);
              //setFinancialValues(response.data.financialResultSet[0]);
              setLoading(false);
              setPeriodInYrsValue(
                Number(response.data.financialResultSet[0].periodInYrs)
              );
              props.setInsurance(response.data.financialResultSet[0].insurance);
              props.setFuelSupply(response.data.financialResultSet[0].fuelSupply);
              props.setPeriodMnths(
                response.data.financialResultSet[0].periodInMnths
              );
              props.setTravellingMnths(
                response.data.financialResultSet[0].travellingpm
              );
              setTravellingpyValue(
                response.data.financialResultSet[0].travellingpy
              );
              props.setDepositPercent(
                Number(response.data.financialResultSet[0].depositInPercent)
              );
              setBalloonInRandValue(
                response.data.financialResultSet[0].balloonInRand
              );
              setDepositInRandValue(
                response.data.financialResultSet[0].depositInRand
              );
              props.setDepositRand(
                response.data.financialResultSet[0].depositInRand
              );
              props.setBalloonPercent(
                Number(response.data.financialResultSet[0].balloonInPercent)
              );
              props.setBalloonRand(
                response.data.financialResultSet[0].balloonInRand
              );
              props.setInterest(
                response.data.financialResultSet[0].interestRate
              );
              props.setMaxPrice(response.data.financialResultSet[0].price);
              props.setMonthlyExpenditure();
              //props.setMonthlyInstallment(response.data.financialResultSet[0].monthlyInstallment);
              
              let data = {
                interest: response.data.financialResultSet[0].interestRate,
                period_in_months: response.data.financialResultSet[0].periodInMnths,
                travelling_in_months: response.data.financialResultSet[0].travellingpm,
                period_in_years: response.data.financialResultSet[0].periodInYrs,
                travelling_in_years: response.data.financialResultSet[0].travellingpy,
                balloon_percent: response.data.financialResultSet[0].balloonInPercent,
                deposit_percent: response.data.financialResultSet[0].depositInPercent,
                insurance: response.data.financialResultSet[0].insurance,
                petrol: props.petrol,
                diesel: props.diesel,
                electric_units: props.elecUnits,
                monthlyIns: response.data.financialResultSet[0].monthlyInstallment,
                monthlyExp: response.data.financialResultSet[0].monthlyExpenditure
              };
          
              props.setFinancial(data);
            })
            .catch((error) => console.log("financial error" + error));
        }

        if (sessionStorage.getItem("financialData") && !financialData) {
          props.isCustomFinancial(true);
          console.log("session data");
          setFinancialData(true);
          fData = JSON.parse(sessionStorage.getItem("financialData"));
          console.log("fData" + JSON.stringify(fData));
          let fdata = JSON.parse(sessionStorage.getItem("financialData"));
          //setFinancialValues(fdata[0]);
          setLoading(false);
          setPeriodInYrsValue(fdata[0].periodInYrs);
          props.setPeriodMnths(fdata[0].periodInMnths);
          props.setTravellingMnths(fdata[0].travellingpm);
          setTravellingpyValue(fdata[0].travellingpy);
          props.setInsurance(fData[0].insurance);
          props.setFuelSupply(fData[0].fuelSupply);
          props.setDepositPercent(fdata[0].depositInPercent);
          props.setDepositRand(fdata[0].depositInRand);
          props.setBalloonPercent(fdata[0].balloonInPercent);
          props.setBalloonRand(fdata[0].balloonInRand);
          setBalloonInRandValue(fdata[0].balloonInRand);
          setDepositInRandValue(fdata[0].depositInRand);
          props.setInterest(fdata[0].interestRate);
          console.log("here" + props.interest);
          props.setMaxPrice(fdata[0].price);
          props.setMonthlyExpenditure();
          //props.setMonthlyInstallment(fdata[0].monthlyInstallment);
          let data = {
            interest: fdata[0].interestRate,
            period_in_months: fdata[0].periodInMnths,
            travelling_in_months: fdata[0].travellingpm,
            period_in_years: fdata[0].periodInYrs,
            travelling_in_years: fdata[0].travellingpy,
            balloon_percent: fdata[0].balloonInPercent,
            deposit_percent: fdata[0].depositInPercent,
            insurance: fData[0].insurance,
            petrol: props.petrol,
            diesel: props.diesel,
            electric_units: props.elecUnits,
            monthlyIns: fdata[0].monthlyInstallment,
            monthlyExp: fdata[0].monthlyExpenditure
          };
      
          props.setFinancial(data);
        }
      })
      .catch((error) => console.log(error));
  };
  const setData = () => {
    console.log("after calls" + props.balloonPercent);
    let data = {
      interest: props.interest,
      period_in_months: props.periodMnths,
      travelling_in_months: props.travellingMnths,
      period_in_years: periodInYrsValue,
      travelling_in_years: travellingpyValue,
      balloon_percent: props.balloonPercent,
      deposit_percent: props.depositPercent,
      insurance: props.insurance,
      petrol: props.petrol,
      diesel: props.diesel,
      electric_units: props.elecUnits,
      monthlyIns: props.monthlyInstallment,
      monthlyExp: props.monthlyExpenditure
    };

    props.setFinancial(data);
  };
  const handleOpen = () => {
    setOpen(true);
    setDepositInRandValue(props.depositRand);
    setBalloonInRandValue(props.balloonRand);
  };

  const handleClose = () => {
    setOpen(false);
    setSuccessful(false);
  };

  const handleClick = () => {
    setOpen(false);
    setSuccessful(false);
  };

  const handlePeriodInYrs = (event) => {
    setPeriodInYrsValue(event.target.value);
    handlePeriodInYrsToMnths(event.target.value);
  };

  const setPeriodInYearsOnBlur = (event) => {
    if (
      event.target.value === "0" ||
      event.target.value == null ||
      event.target.value === "" ||
      event.target.value === 0
    ) {
      setPeriodInYrsValue(
        Number((props.financial.period_in_months / 12).toFixed(1))
      );
      handlePeriodInYrsToMnths(
        Number((props.financial.period_in_months / 12).toFixed(1))
      );
    }
  };

  const handlePeriodInYrsToMnths = (yearVal) => {
    const monthConversionValue = yearVal ? yearVal * 12 : props.periodMnths;
    props.setPeriodMnths(monthConversionValue);
    props.setMonthlyExpenditure();
  };

  const handlePeriodInMnths = (event) => {
    props.setPeriodMnths(event.target.value);
    props.setMonthlyExpenditure();
    handlePeriodInMnthsToYrs(event.target.value);
  };

  const setPeriodInMonthsOnBlur = (event) => {
    if (
      event.target.value === "0" ||
      event.target.value == null ||
      event.target.value === "" ||
      event.target.value === 0
    ) {
      props.setPeriodMnths(props.financial.period_in_months);
      props.setMonthlyExpenditure();
      handlePeriodInMnthsToYrs(props.financial.period_in_months);
    }
  };

  const handlePeriodInMnthsToYrs = (monVal) => {
    setPeriodInYrsValue("");
    const yearConversionValue =
      monVal > 0 ? Number((monVal / 12).toFixed(2)) : periodInYrsValue;
    setPeriodInYrsValue(yearConversionValue);
  };

  const handleInsurance = (event) => {
    const insuranceR = currencyFormatter.unformat(event.target.value, {
      code: "ZAR",
    });
    props.setInsurance(insuranceR);
    props.setMonthlyExpenditure();
  };

  const handleTravellingpm = (event) => {
    props.setTravellingMnths(event.target.value);
    props.setMonthlyExpenditure();
    handleTravellingpmTopy(event.target.value);
  };

  const handleTravellingpmTopy = (travelPm) => {
    setTravellingpyValue("");
    const travellingpmTopyConversionValue = travelPm
      ? travelPm * 12
      : travellingpyValue;
    setTravellingpyValue(travellingpmTopyConversionValue);
  };

  const handleTravellingpy = (event) => {
    setTravellingpyValue(event.target.value);
    handleTravellingpyTopm(event.target.value);
  };

  const handleTravellingpyTopm = (travelPy) => {
    const travellingpyTopmConversionValue = travelPy
      ? Math.round(travelPy / 12)
      : props.travellingMnths;
    props.setTravellingMnths(travellingpyTopmConversionValue);
    props.setMonthlyExpenditure();
  };

  const handleDepositInPercent = (event) => {
    props.setDepositPercent(event.target.value);
    handleDepositInPercentToRand(event.target.value);
  };

  const handleDepositInPercentToRand = (depositPercentVal) => {
    setDepositInRandValue("");
    const depositInPercentToRandConversionValue = Math.round(
      (props.maxPrice * depositPercentVal) / 100
    );
    props.setDepositRand(depositInPercentToRandConversionValue);
    setDepositInRandValue(depositInPercentToRandConversionValue);
  };

  const handleDepositInRand = (event) => {
    const depositR = currencyFormatter.unformat(event.target.value, {
      code: "ZAR",
    });

    props.setDepositRand(depositR);
    setDepositInRandValue(depositR);
    handleDepositInRandToPercent(depositR);
  };

  const handleDepositInRandToPercent = (depositRandVal) => {
    const depositInRandToPercentConversionValue = Number(
      ((depositRandVal / props.maxPrice) * 100).toFixed(2)
    );
    props.setDepositPercent(depositInRandToPercentConversionValue);
  };

  const handleBalloonInPercent = (event) => {
    props.setBalloonPercent(event.target.value);
    handleBalloonInPercentToRand(event.target.value);
  };

  const handleBalloonInPercentToRand = (balloonPercentVal) => {
    setBalloonInRandValue("");
    const balloonInPercentToRandConversionValue = Math.round(
      (props.maxPrice * balloonPercentVal) / 100
    );
    props.setBalloonRand(balloonInPercentToRandConversionValue);
    setBalloonInRandValue(balloonInPercentToRandConversionValue);
  };

  const handlBalloonInRand = (event) => {
    const balloonR = currencyFormatter.unformat(event.target.value, {
      code: "ZAR",
    });

    props.setBalloonRand(balloonR);
    setBalloonInRandValue(balloonR);
    handleBalloonInRandToPercent(balloonR);
  };

  const handleBalloonInRandToPercent = (balloonRandVal) => {
    const balloontInRandToPercentConversionValue = Number(
      ((balloonRandVal / props.maxPrice) * 100).toFixed(2)
    );
    props.setBalloonPercent(balloontInRandToPercentConversionValue);
  };
  const handleMonthlyInstallment = (monthlyInstallval) => {
    const mi = parseInt(monthlyInstallval.toFixed(2));
    console.log("MonthlyInstallval" + monthlyInstallval);
    props.setMonthlyInstallment(mi);
    props.setMonthlyExpenditure();
  };
  const check = (e) => {
    console.log("checkMI");
  };
  const calculation = (event) => {
    //console.log("value"+price+ depositInRandValue+balloonInRandValue+interestRate +periodInMnthsValue   );
    const miValue =
      (((props.maxPrice - depositInRandValue) *
        Math.pow(1 + props.interest / 1200, props.periodMnths) -
        balloonInRandValue) *
        (props.interest / 1200)) /
      (Math.pow(1 + props.interest / 1200, props.periodMnths) - 1);
    handleMonthlyInstallment(miValue);
  };

  const saveInterestRate = (event) => {
    props.setInterest(event.target.value);
    props.setMonthlyExpenditure();
  };

  const setInterestOnBlur = (event) => {
    if (
      event.target.value === "0" ||
      event.target.value == null ||
      event.target.value === "" ||
      event.target.value === 0
    ) {
      props.setInterest(props.financial.interest);
      props.setMonthlyExpenditure();
    }
  };

  const saveData = (e) => {
    setLoading(true);
    setFinancialData(true);
    props.isCustomFinancial(true);
    if (sessionStorage.getItem("user")) {
      let token = getToken();
      let user = JSON.parse(sessionStorage.getItem("user"));
      setSuccessful(true);
      axios({
        method: "post",
        url: `${process.env.REACT_APP_API_URL}data/setfinancial`,
        data: {
          interestRate: props.interest,
          periodInMnths: props.periodMnths,
          depositInRand: props.depositRand,
          balloonInRand: props.balloonRand,
          travellingpm: props.travellingMnths,
          periodInYrs: periodInYrsValue,
          travellingpy: travellingpyValue,
          monthlyInstallment: props.monthlyInstallment,
          monthlyExpenditure:props.monthlyExpenditure,
          balloonInPercent: props.balloonPercent,
          depositInPercent: props.depositPercent,
          insurance: props.insurance,
          buyerId: user.id,
          price: props.maxPrice,
          fuelSupply:props.fuelSupply
        },
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          console.log("login response: ", response);
          setLoading(false);
          if (response.status === 200) {
            console.log("data saved");
            setSuccessful(true);

            // browserHistory.push("/");
          }
        })
        .catch((err) => {
          //handle error
          console.log("login error", err);
        });
    } else {
      setLoading(false);
      let data = [
        {
          interestRate: props.interest,
          periodInMnths: props.periodMnths,
          depositInRand: props.depositRand,
          balloonInRand: props.balloonRand,
          travellingpm: props.travellingMnths,
          periodInYrs: periodInYrsValue,
          travellingpy: travellingpyValue,
          monthlyInstallment: props.monthlyInstallment,
          monthlyExpenditure: props.monthlyExpenditure,
          balloonInPercent: props.balloonPercent,
          depositInPercent: props.depositPercent,
          insurance: props.insurance,
          price: props.maxPrice,
          fuelSupply:props.fuelSupply
        },
      ];
      setFinancialSession(data);
      setSuccessful(true);
    }
    setData();
    setShowSavedMsg(true);
    setTimeout(() => {
      setOpen(false);
      setShowSavedMsg(false);
    }, 3000);
  };

  const body = (
    <div className="cardmodal" id="financial_popup_id">
      <div style={modalStyle} className="FinancialModalPaper">
        <h2 id="simple-modal-title-financial">Financials</h2>
        <p id="simple-modal-title-para">Change these default parameters to suit your personal needs</p>
        <Chip label="x" className={classes.closebtn} onClick={handleClick} />
        {loading && (
          <Alert severity="info">
            <AlertTitle>Saving...</AlertTitle>
          </Alert>
        )}
        <p id="simple-modal-description" class="finacialmodal">
          <Scrollbars style={{ height: 365 }}>

            <Grid container>
              <Grid item xs={12} sm={12} md={4} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Price(R)"
                  variant="outlined"
                  className="modalfields notation"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  value={currencyFormatter
                    .format(props.maxPrice, { code: "ZAR" })
                    .replace(",00", "")
                    .replace("R", "")}
                  disabled={disabled}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Monthly Installment(R)"
                  variant="outlined"
                  className="modalfields notation"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  disabled={disabled}
                  value={
                    isFinite(Math.round(props.monthlyInstallment)) &&
                      !isNaN(Math.round(props.monthlyInstallment))
                      ? currencyFormatter
                        .format(Math.round(props.monthlyInstallment), {
                          code: "ZAR",
                        })
                        .replace(",00", "")
                        .replace("R", "")
                      : "-"
                  }
                  onChange={check}
                />
              </Grid>

              <Grid item xs={12} sm={12} md={4} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Monthly Expenditure(R)"
                  variant="outlined"
                  className="modalfields notation"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  value={
                    isFinite(Math.round(props.monthlyExpenditure)) &&
                      !isNaN(Math.round(props.monthlyExpenditure))
                      ? currencyFormatter
                        .format(Math.round(props.monthlyExpenditure), {
                          code: "ZAR",
                        })
                        .replace(",00", "")
                        .replace("R", "")
                      : "-"
                  }
                  disabled={disabled}
                />
              </Grid>
            </Grid>

            <Grid container>
              <Grid item xs={12} sm={12} md={4}>
                <Radio
                  checked={props.fuelSupply === 'petrol'}
                  onChange={handleRadioChange}
                  value="petrol"
                  name="radio-button-demo"
                  inputProps={{ 'aria-label': 'Petrol' }}
                />

                <Typography variant="caption" display="block" gutterBottom className="fuelSelectionRadio"
                  style={{ display: "inline-block", marginRight: 5 }}>
                  Petrol 95 R/Litre
                </Typography>
                <TextField
                  id="outlined-basic"
                  variant="outlined"
                  className="modalfields radiofields"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  disabled={disabled}
                  defaultValue={currencyFormatter
                    .format(financialValues.petrol95, { code: "ZAR" })
                    .replace(",00", "")
                    .replace("R", "")} />
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Radio
                  checked={props.fuelSupply === 'electric'}
                  onChange={handleRadioChange}
                  value="electric"
                  name="radio-button-demo"
                  inputProps={{ 'aria-label': 'Electric' }}
                />
                <Typography variant="caption" display="block" gutterBottom className="fuelSelectionRadio"
                  style={{ display: "inline-block", marginRight: 5 }}>
                  Electric Units R/Unit
                </Typography>
                <TextField
                  id="outlined-basic"
                  variant="outlined"
                  className="modalfields radiofields"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  disabled={disabled}
                  defaultValue={currencyFormatter
                    .format(financialValues.electUnits, { code: "ZAR" })
                    .replace(",00", "")
                    .replace("R", "")}
                />
              </Grid>

              <Grid item xs={12} sm={12} md={4} >
                <Radio
                  checked={props.fuelSupply === 'diesel'}
                  onChange={handleRadioChange}
                  value="diesel"
                  name="radio-button-demo"
                  inputProps={{ 'aria-label': 'Diesel' }}
                />
                <Typography variant="caption" display="block" gutterBottom className="fuelSelectionRadio"
                  style={{ display: "inline-block", marginRight: 5 }}>
                  Diesel 50ppm R/Litre
                </Typography>
                <TextField
                  id="outlined-basic"
                  variant="outlined"
                  className="modalfields radiofields"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  disabled={disabled}
                  defaultValue={currencyFormatter
                    .format(financialValues.diesel50ppm, { code: "ZAR" })
                    .replace(",00", "")
                    .replace("R", "")}
                />
              </Grid>


            </Grid>

            <Grid container>
              <Grid item xs={12} sm={12} md={4} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Interest Rate (in %)"
                  variant="outlined"
                  className="modalfields"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">%</InputAdornment>
                    ),
                  }}
                  value={props.interest}
                  onChange={saveInterestRate}
                  onBlur={setInterestOnBlur}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Insurance(R)"
                  variant="outlined"
                  className="modalfields notation"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  value={currencyFormatter
                    .format(props.insurance, { code: "ZAR" })
                    .replace(",00", "")
                    .replace("R", "")}
                  onChange={handleInsurance}
                />
              </Grid>

              <Grid item xs={12} sm={12} md={4} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Fuel Consumption"
                  variant="outlined"
                  className="modalfields"
                  disabled={disabled}
                  defaultValue={props.fuelCons}
                />
              </Grid>
            </Grid>



            <Grid container>
              <Grid item xs={12} sm={12} md={3} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Period(in yr)"
                  variant="outlined"
                  className="modalfields"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">Year(s)</InputAdornment>
                    ),
                  }}
                  value={periodInYrsValue}
                  onChange={handlePeriodInYrs}
                  onBlur={setPeriodInYearsOnBlur}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={3} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Period(in months)"
                  variant="outlined"
                  className="modalfields"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">Month(s)</InputAdornment>
                    ),
                  }}
                  value={props.periodMnths}
                  onChange={handlePeriodInMnths}
                  onBlur={setPeriodInMonthsOnBlur}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={3} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Deposit(in %)"
                  variant="outlined"
                  className="modalfields"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">%</InputAdornment>
                    ),
                  }}
                  value={props.depositPercent}
                  onChange={handleDepositInPercent}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={3} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Deposit(R)"
                  variant="outlined"
                  className="modalfields notation"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  value={currencyFormatter
                    .format(props.depositRand, { code: "ZAR" })
                    .replace(",00", "")
                    .replace("R", "")}
                  onChange={handleDepositInRand}
                />
              </Grid>

            </Grid>

            <Grid container>
              <Grid item xs={12} sm={12} md={3} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Balloon(in %)"
                  variant="outlined"
                  className="modalfields"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">%</InputAdornment>
                    ),
                  }}
                  value={props.balloonPercent}
                  onChange={handleBalloonInPercent}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={3} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Balloon(R)"
                  variant="outlined"
                  className="modalfields notation"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  value={currencyFormatter
                    .format(props.balloonRand, { code: "ZAR" })
                    .replace(",00", "")
                    .replace("R", "")}
                  onChange={handlBalloonInRand}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={3} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Travelling(km/y)"
                  variant="outlined"
                  className="modalfields"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">km/y</InputAdornment>
                    ),
                  }}
                  value={travellingpyValue}
                  onChange={handleTravellingpy}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={3} style={{ marginTop: 12 }}>
                <TextField
                  id="outlined-required"
                  label="Travelling (km/m)"
                  variant="outlined"
                  className="modalfields"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">km/m</InputAdornment>
                    ),
                  }}
                  value={props.travellingMnths}
                  onChange={handleTravellingpm}
                />
              </Grid>
            </Grid>

          </Scrollbars>

          <Grid container>
            <Grid item xs style={{ marginRight: 10, textAlign: "right" }}>
              <Button
                variant="contained"
                className={classes.textbtns}
                onClick={saveData}
              >
                Save
              </Button>
              <Button
                variant="contained"
                className={classes.textbtns}
                onClick={handleClick}
                style={{ marginRight: 0 }}>

                Cancel
              </Button>
            </Grid>

          </Grid>
        </p>
        {successful}
        {showSavedMsg && (
          <Alert severity="info">
            <AlertTitle> Saved Successfully...</AlertTitle>
          </Alert>
        )}
      </div>
    </div>
  );

  return (
    <div>
      <Button
        variant="contained"
        onClick={handleOpen}
        color="primary"
        size="small"
        style={{
          marginRight: 10,
          textTransform: "capitalize",
          fontSize: 14,
          marginTop: 20,
        }}
      >
        Financials
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
};

const mapStateToProps = (state) => ({
  loading: state.loading,
  maxPrice: state.maxPrice,
  interest: state.interest,
  balloonPercent: state.balloonPercent,
  depositPercent: state.depositPercent,
  balloonRand: state.balloonRand,
  depositRand: state.depositRand,
  periodMnths: state.periodMnths,
  monthlyInstallment: state.monthlyInstallment,
  customFinancial: state.customFinancial,
  insurance: state.insurance,
  travellingMnths: state.travellingMnths,
  diesel: state.diesel,
  petrol: state.petrol,
  fuelSupply: state.fuelSupply,
  monthlyExpenditure: state.monthlyExpenditure,
  fuelCons: state.fuelCons,
  elecUnits: state.elecUnits,
  financial: state.financial,
  fuelSupply: state.fuelSupply
});

const mapDispatchToProps = (dispatch) => ({
  setMaxPrice: (value) => dispatch(setMaxPrice(value)),
  setDepositRand: (value) => dispatch(setDepositRand(value)),
  setBalloonRand: (value) => dispatch(setBalloonRand(value)),
  setInterest: (value) => dispatch(setInterest(value)),
  setPeriodMnths: (value) => dispatch(setPeriodMnths(value)),
  setBalloonPercent: (value) => dispatch(setBalloonPercent(value)),
  setDepositPercent: (value) => dispatch(setDepositPercent(value)),
  setMonthlyInstallment: (value) => dispatch(setMonthlyInstallment(value)),
  isCustomFinancial: (value) => dispatch(isCustomFinancial(value)),
  setTravellingMnths: (value) => dispatch(setTravellingMnths(value)),
  setInsurance: (value) => dispatch(setInsurance(value)),
  setDiesel: (value) => dispatch(setDiesel(value)),
  setPetrol: (value) => dispatch(setPetrol(value)),
  setFuelSupply: (value) => dispatch(setFuelSupply(value)),
  setMonthlyExpenditure: () => dispatch(setMonthlyExpenditure()),
  setFuelCons: (value) => dispatch(setFuelCons(value)),
  setElecUnits: (value) => dispatch(setElecUnits(value)),
  setFinancial: (value) => dispatch(setFinancial(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FinancialsModal);
