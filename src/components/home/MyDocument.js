import React from "react";
import { Page, Text, View, Document, StyleSheet } from "@react-pdf/renderer";
import currencyFormatter from "currency-formatter";
// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: "column",
    backgroundColor: "#ffffff",
  },
  section: {
    margin: 50,
    padding: 50,
    flexGrow: 1,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    color: "#9C1233",
    marginBottom: 10,
  },
  subtitle: {
    fontSize: 11,
    color: "#9C1233",
    textTransform: "uppercase",
    marginTop: 10,
    marginBottom: 10,
    fontWeight: "bold",
    flexDirection: "row",
  },
  content: { fontSize: 10, marginBottom: 5 },

  cardetails: {
    display: "flex",
    margin: 20,
  },
  image: {
    height: 200,
    width: 100,
  },
  firstColumn: {
    width: "50%",
  },
  filterKeyColumn: {
    width: "30%",
  },
  filterValueColumn: {
    width: "70%",
  },
  body: {
    flexGrow: 1,
  },
  row: {
    flexGrow: 1,
    flexDirection: "row",
  },
  subtitle1: {
    backgroundColor: "#333",
    color: "#ffffff",
    textTransform: "uppercase",
    fontSize: 11,
    marginBottom: 10,
    fontWeight: "bold",
    padding: 10,
    flexDirection: "row",
  },
  item: {
    flexDirection: "row",
    marginBottom: 2,
  },
  bulletPoint: {
    width: 10,
    fontSize: 10,
    marginLeft: 10,
  },
  bulletPointColor: {
    width: 10,
    fontSize: 10,
    marginLeft: 10,
    color: "#9C1233",
  },
  secondRow: {
    width: "50%",
    marginTop: 20,
  },
  filter: {
    fontSize: 10,
    marginBottom: 5,
    color: "#9C1233",
    flexDirection: "column",
    textTransform: "uppercase",
  },
});

// Create Document Component
const MyDocument = (props) => (
  <Document>
    <Page size="A4">
      <View style={styles.cardetails}>
        {/* *************Applied Filters**************** */}
        <View style={styles.body}>
          <Text style={styles.subtitle1}>{"Applied Filters"}</Text>
          {/* Filters - Left */}
          <View style={styles.row}>
            <View style={styles.filterKeyColumn}>
              {Object.keys(props.appliedFilters).map((key, i) => (
                <View style={styles.item}>
                  <Text style={styles.bulletPointColor}>•</Text>
                  <Text style={styles.filter}>
                    {key.replace("Sensor", "").replaceAll("_", " ") + ":"}
                  </Text>
                  {/* // */}
                </View>
              ))}
            </View>

            <View style={styles.filterValueColumn}>
              {Object.keys(props.appliedFilters).map((key, i) => (
                <View style={styles.item}>
                  {key === "PriceSensor" ||
                  key === "InstallmentSensor" ||
                  key === "monthly_ExpSensor" ? (
                    <Text style={styles.content}>
                      {" MIN-" +
                        props.appliedFilters[key].value[0] +
                        " & MAX-" +
                        props.appliedFilters[key].value[1]}
                    </Text>
                  ) : (
                    <Text style={styles.content}>
                      {"" + props.appliedFilters[key].value.join(", ")}
                    </Text>
                  )}
                </View>
              ))}
            </View>
          </View>
        </View>
      </View>

      <View style={styles.cardetails}>
        {/* *************Preference data**************** */}
        <View style={styles.body}>
          <Text style={styles.subtitle1}>{"Applied Preferences"}</Text>
          {/* Preferences - Left */}
          <View style={styles.row}>
            <View style={styles.filterKeyColumn}>
              {Object.keys(props.preferencesData).map((key, i) => (
                <>
                  {props.preferencesData[key].visibility === "default" && (
                    <>
                      <View style={styles.item}>
                        <Text style={styles.bulletPointColor}>•</Text>
                        <Text style={styles.filter}>
                          {props.preferencesData[key].label + ":"}
                        </Text>
                      </View>
                    </>
                  )}
                </>
              ))}
            </View>

            <View style={styles.filterKeyColumn}>
              {Object.keys(props.preferencesData).map((key, i) => (
                <>
                  {props.preferencesData[key].visibility === "default" && (
                    <>
                      <View style={styles.item}>
                        <Text style={styles.content}>
                          {props.preferencesData[key].defaultValue}
                        </Text>
                      </View>
                    </>
                  )}
                </>
              ))}
            </View>
          </View>
        </View>
      </View>

      <View style={styles.cardetails}>
        {/* *************Financial data**************** */}
        <View style={styles.body}>
          <Text style={styles.subtitle1}>{"Financials"}</Text>
          {/* Financial - Left */}
          <View style={styles.row}>
            <View style={styles.filterKeyColumn}>
              {Object.keys(props.financialData).map((key, i) => (
                <>
                  <>
                    <View style={styles.item}>
                      <Text style={styles.bulletPointColor}>•</Text>
                      <Text style={styles.filter}>
                        {key.replaceAll("_", " ") + ":"}
                      </Text>
                    </View>
                  </>
                </>
              ))}
            </View>

            <View style={styles.filterKeyColumn}>
              {Object.keys(props.financialData).map((key, i) => (
                <>
                  <>
                    <View style={styles.item}>
                      <Text style={styles.content}>
                        {props.financialData[key]}
                      </Text>
                    </View>
                  </>
                </>
              ))}
            </View>
          </View>
        </View>
      </View>
    </Page>
    {/* *************Page 2**************** */}
    {props.modelItems.map((item) => (
      <Page size="A4">
        <View style={styles.cardetails}>
          <Text style={styles.title}>
            {" "}
            {item.model_data.manufacturer.name}
            {" " + item.model_data.modelName}
          </Text>

          {/* *************key specification**************** */}
          <View style={styles.body}>
            <Text style={styles.subtitle1}>{"Key Specifications"}</Text>

            {/* General */}
            <View style={styles.row}>
              <View style={styles.firstColumn}>
                <Text style={styles.subtitle}>{"General"}</Text>

                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Doors: " + item.model_data.doors}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Seats: " + item.model_data.seats}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Vehicle Dimensions: " + item.model_data.vehicleDimensions}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Price: " +
                      currencyFormatter
                        .format(item.model_data.price, { code: "ZAR" })
                        .replace(",00", "").replace("R", "R ")}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Parts Prices: " +
                      currencyFormatter
                        .format(item.model_data.partsPriceTotal, {
                          code: "ZAR",
                        })
                        .replace(",00", "").replace("R", "R ")}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Top Speed: " + item.model_data.maxSpeed}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Speed Calibration: " + item.model_data.speedCalibration}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Acceleration: " + item.model_data.accelDetails}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Overtaking Acceleration: " +
                      item.model_data.OTaccelDetails}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Fuel Details: " + item.model_data.fuelDetails}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Feature List: " + item.model_data.featuresList}
                  </Text>
                </View>
              </View>

              {/* ENGINE */}
              <View style={styles.firstColumn}>
                <Text style={styles.subtitle}>{"Engine"}</Text>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Cylinders: " + item.model_data.engineConfig.name}
                    {", " + item.model_data.engineDirection.name}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Fuel Supply: " + item.model_data.fuelSuplDetails}
                    {", " + item.model_data.fuelSupply.name}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {}
                    {"Cubic Capacity: " + item.model_data.engineCap}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Engine Bore/Stroke: " + item.model_data.engineBoreStroke}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Valve Gear: " + item.model_data.valveGear}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Engine Compression Ratio: " +
                      item.model_data.engineCompRatio}
                  </Text>
                </View>
              </View>
            </View>

            {/* ENGINE OUTPUT */}
            <View style={styles.row}>
              <View style={styles.firstColumn}>
                <Text style={styles.subtitle}>{"Engine Output"}</Text>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Max Power ISO: " + item.model_data.power_kw}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Power Peak/Red line: " + item.model_data.fuelSuplDetails}
                    {", " + item.model_data.power_rpm}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {}
                    {"Max Torque: " + item.model_data.torque_nm}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Torque Peak: " + item.model_data.torque_rpm}
                  </Text>
                </View>
              </View>
              {/* TRANSMISSION */}
              <View style={styles.firstColumn}>
                <Text style={styles.subtitle}>{"Transmission"}</Text>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Type: " + item.model_data.transmission}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Gears: " + item.model_data.gears}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Transmission Details: " + item.model_data.transmDetails}
                  </Text>
                </View>
              </View>
            </View>

            {/* Brakes */}
            <View style={styles.row}>
              <View style={styles.firstColumn}>
                <Text style={styles.subtitle}>{"Brakes"}</Text>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Abs: " + item.model_data.abs}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Type: " + item.model_data.brakes}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Brake Avg: " + item.model_data.brakingAvg}
                  </Text>
                </View>
              </View>
              {/* Wheels and tyres */}
              <View style={styles.firstColumn}>
                <Text style={styles.subtitle}>{"Wheels and tyres"}</Text>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Type: " + item.model_data.wheelsTyres}
                  </Text>
                </View>
              </View>
            </View>

            {/* Steering */}
            <View style={styles.row}>
              <View style={styles.firstColumn}>
                <Text style={styles.subtitle}>{"Steering"}</Text>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Type: " + item.model_data.steeringDetails}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Turning Circle Diameter: " +
                      item.model_data.turningCircle}
                  </Text>
                </View>
              </View>

              {/* Suspension */}
              <View style={styles.firstColumn}>
                <Text style={styles.subtitle}>{"Suspension"}</Text>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Type: " + item.model_data.suspension}
                  </Text>
                </View>
              </View>
            </View>

            {/* *************Fuel Consumption************* */}
            <View style={styles.row}>
              <View style={styles.firstColumn}>
                <Text style={styles.subtitle}>{"Fuel Consumption"}</Text>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Fuel Capacity: " + item.model_data.fuelCapacity}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Taxable CO2: " + item.model_data.CO2}
                  </Text>
                </View>
              </View>

              {/* Warranty & Service Intervals */}
              <View style={styles.firstColumn}>
                <Text style={styles.subtitle}>
                  {"Warranty & Service Intervals"}
                </Text>
                <View style={styles.item}>
                  <Text style={styles.bulletPoint}>•</Text>
                  <Text style={styles.content}>
                    {"Warranty: " + item.model_data.warranty}
                  </Text>
                </View>
              </View>
            </View>
          </View>

          {/* *************Features checklist************* */}
          <View style={styles.row}>
            <View style={styles.secondRow}>
              <Text style={styles.subtitle1}>{"Features checklist"}</Text>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"Airbags: " + item.model_data.airbags}
                  {", " + item.model_data.airbagInfo}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"Air-con: " + item.model_data.aircon}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"Auto Light: " + item.model_data.autoLights}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"ABS: " + item.model_data.abs}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"Bluetooth: " + item.model_data.bluetooth}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"Air Con: " + item.model_data.aircon}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"ESP: " + item.model_data.ESP}
                </Text>
              </View>
            </View>
            {/* *************Performance Factors************* */}
            <View style={styles.secondRow}>
              <Text style={styles.subtitle1}>{"Performance Factors"}</Text>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"Power/mass: " + item.model_data.powerToMass}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"Power/litre: " + item.model_data.powerPerLitre}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"Torque/litre: " + item.model_data.torquePerLitre}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"Cabin Noise Level at Idle: " + item.model_data.noiseLevel}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.bulletPointColor}>•</Text>
                <Text style={styles.content}>
                  {"Engine Speed Max: " + item.model_data.engineSpeed120}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
    ))}
  </Document>
);

export default MyDocument;
