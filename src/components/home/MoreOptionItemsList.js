import React from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import MuseumIcon from "@material-ui/icons/Museum";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import { connect } from "react-redux";
import {
  adddefaultFilter,
  remove_optional_filters,
} from "../../actions/actionCreators";
import { Scrollbars } from "react-custom-scrollbars";

const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
  leftIcon: {
    fontSize: 14,
  },
  rightIcon: {
    fontSize: 14,
  },

  poppverbtns: {
    textTransform: "capitalize",
    borderColor: "#9c1233",
    width: "100%",
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },

  iconpoppverbtns: {
    textTransform: "capitalize",
    borderColor: "#9c1233",
    width: 45,
    color: "#00000042",
  },

  textbtns: {
    textTransform: "capitalize",
    borderColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },

  onchangetextbtns: {
    textTransform: "capitalize",
    backgroundColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
      backgroundColor: "#9c1233",
    },
  },

  closebtn: {
    position: "absolute",
    right: 7,
    top: 13,
    color: "#4caf50",
    fontSize: 18,
    cursor: "pointer",
  },
}));

function MoreOptionItemsList(props) {
  const classes = useStyles();
  //const [btnActive, setBtnActive] = React.useState("buttonActive");
  const handleClick = (scope) => {
    console.log(scope);
    props.addFilters(scope);
    props.removeOptional(scope.id);
  };

  let scopeValues = props.optionalFilters;
  return (
    <div>
      <Grid container direction="column" alignItems="center">
        <Grid item sm={12} style={{ width: "100%" }}>
         <Scrollbars style={{ height: 500 }}>
            {Object.keys(scopeValues)
              .map((key) => ({ id: key, ...scopeValues[key] }))
              .filter((scope) => scope.visibility === "optional")
              .sort((a, b) => (a.seqNo > b.seqNo ? 1 : -1))
              .map((scope) => (
                <div
                  style={{ position: "relative", paddingRight: "13px" }}
                  key={"more_" + scope.id}
                >
                  <ButtonGroup
                    className={classes.poppverbtns}
                    variant="contained"
                    aria-label="split button"
                    style={{ marginTop: 20 }}
                  >
                    <Button className={classes.iconpoppverbtns} size="small">
                      <MuseumIcon />
                    </Button>
                    <Button
                      disabled
                      variant="contained"
                      className={classes.onchangetextbtns}
                      /*  className={
                  btnActive === "changeBtnColor"
                    ? classes.onchangetextbtns
                    : classes.onchangetextbtns
                } */
                      key={scope.seqNo}
                    >
                      {scope.name}
                    </Button>
                  </ButtonGroup>
                  <AddCircleIcon
                    className={classes.closebtn}
                    onClick={() => handleClick(scope)}
                  />
                </div>
              ))}
          </Scrollbars>
        </Grid>
      </Grid>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    defaultScopes: state.defaultfilters,
    optionalFilters: state.optionalFilters,
  };
};

const mapDispatchToProps = (dispatch) => ({
  addFilters: (value) => dispatch(adddefaultFilter(value)),
  removeOptional: (value) => dispatch(remove_optional_filters(value)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MoreOptionItemsList);
