import React, { useEffect } from "react";
import {
  withStyles,
  createMuiTheme,
  ThemeProvider,
} from "@material-ui/core/styles";
//styles-import
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Preference from "./Preference";
import PreferenceModal from "./PreferenceModal";
import CompleteModal from "./CompleteModal";
import FinancialsModal from "./FinancialsModal";
import { makeStyles } from "@material-ui/styles";
import CardetailModal from "../carDetail/CardetailModal";
import Avatar from "@material-ui/core/Avatar";
import Sidebar from "./Sidebar";
import {
  ReactiveBase,
  ReactiveList,
  SelectedFilters,
} from "@appbaseio/reactivesearch";
import { connect, useDispatch } from "react-redux";
import CircularProgress from "@material-ui/core/CircularProgress";
import {
  getPercentile,
  fetchedDefaultPreferences,
  fetchedDefaultFilters,
  setTotal,
} from "../../actions/actionCreators";
import currencyFormatter from "currency-formatter";
import { useLocation, useHistory } from "react-router-dom";
import queryString from "query-string";
import { getToken } from "../Utils/Common";
import axios from "axios";
import MuiAlert from "@material-ui/lab/Alert";
import {
  fetchScopes,
  fetchPreferences,
  showAnalysisToast,
} from "../../actions/actionCreators";
import Text from "react-texty";
import "react-texty/styles.css";
import { toggleSignIn } from "../../actions/actionCreators";
import { Table as AntTable } from "antd";
import { RightOutlined, DownOutlined } from "@ant-design/icons";
import { Scrollbars } from "react-custom-scrollbars";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
//theme

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#9c1233 !important",
      borderColor: "#9c1233",
      backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    },
  },
  overrides: {
    MuiTableCell: {
      root: {
        padding: "5px",
      },
    },
  },
});

//styles
const useStyles = () => ({
  accorheading: {
    fontSize: 16,
    color: "#4f4f4f",
  },
  sidebaraccordion: {
    marginTop: "0 !important",
    marginBottom: "0 !important",
    "& .MuiAccordionSummary-root.Mui-expanded": {
      minHeight: "40",
    },
    "& .MuiAccordionSummary-content.Mui-expanded": {
      marginTop: 10,
      marginBottom: 10,
    },
  },

  maxvalue: {
    borderWidth: 1,
    borderColor: "#7d7d7f",
    borderStyle: "solid",
    borderRadius: 5,
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottom: "none",
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none",
    },
    "& .MuiInput-underline:before": {
      borderBottom: "none",
    },

    maxtext: {
      color: "#797979",
      fontSize: "14",

      "& .MuiInputLabel-outlined": {
        lineHeight: 1,
      },
      "& .MuiOutlinedInput-input": {
        paddingLeft: 14,
        paddingRight: 14,
        paddingTop: 13,
        paddingBottom: 13,
      },
    },

    carimg: {
      height: "unset",
      width: "100",
      borderRadius: 0,
    },
    cartxt: {
      color: "#797979",
      fontSize: "14",
      textAlign: "center",
      marginTop: 5,
    },
    cardetail: {
      position: "relative",
    },
    closebtn: {
      position: "absolute",
      top: 0,
      right: 0,
    },

    btncard: {
      paddingTop: "0 !important",
    },
    loader: {
      position: "relative !important",
      left: "50%",
    },
  },
});
const useStylesContainer = makeStyles({
  spacing: {
    paddingRight: 10,
    paddingLeft: 10,
    marginTop: 20,
  },
  m20: { marginTop: 30 },
  loader: {
    position: "absolute !important",
    left: "50%",
    top: "80%",
    zIndex: 999,
  },
});
//const elasticSearchUrl = `${process.env.REACT_APP_ELASTIC_SEARCH_URL}`;

function TableTop(props) {
  return (
    <React.Fragment>
      <Typography
        variant="caption"
        display="block"
        gutterBottom
        style={{
          display: "inline-block",
          fontSize: 16,
          color: "#000",
          marginBottom: 20,
          marginTop: 20,
        }}
      ></Typography>
      <ThemeProvider theme={theme}>
        <ButtonGroup style={{ float: "right" }}>
          <FinancialsModal />
        </ButtonGroup>
      </ThemeProvider>
    </React.Fragment>
  );
}

function createData(item, customFinancial, percentileValues, financial) {
  console.log("works" + financial.interest);
  let modelName =
    item.model_data.manufacturer.name + " " + item.model_data.modelName;
  let price = currencyFormatter
    .format(item.model_data.price, { code: "ZAR" })
    .replace(",00", "")
    .replace("R", "");
  let monthlyInstallment = item.model_data.monthlyInstallment;
  let monthlyInstallmentFinalValue = currencyFormatter
    .format(monthlyInstallment, { code: "ZAR" })
    .replace(",00", "")
    .replace("R", "");
  let monthlyExp = item.model_data.monthlyExp;
  let monthlyExpFinalValue = currencyFormatter
    .format(monthlyExp, { code: "ZAR" })
    .replace(",00", "")
    .replace("R", "");
  if (customFinancial) {
    monthlyInstallment =
      (((item.model_data.price -
        (item.model_data.price * financial.deposit_percent) / 100) *
        Math.pow(1 + financial.interest / 1200, financial.period_in_months) -
        (item.model_data.price * financial.balloon_percent) / 100) *
        (financial.interest / 1200)) /
      (Math.pow(1 + financial.interest / 1200, financial.period_in_months) - 1);
    //console.log("mi"+monthlyInstallment+" p"+item.model_data.fuelSupply.name+" d"+depositPercent+" b"+balloonPercent+" i"+interest+" mn"+periodMnths);
    monthlyInstallment = Math.round(monthlyInstallment);
    monthlyInstallmentFinalValue = currencyFormatter
      .format(monthlyInstallment, { code: "ZAR" })
      .replace(",00", "")
      .replace("R", "");

    if (item.model_data.fuelSupply.name === "Petrol") {
      monthlyExp =
        (financial.travelling_in_months / 100) *
        item.model_data.fuelCons *
        financial.petrol +
        monthlyInstallment +
        parseInt(financial.insurance);
      monthlyExp = Math.round(monthlyExp);
      monthlyExpFinalValue = currencyFormatter
        .format(monthlyExp, { code: "ZAR" })
        .replace(",00", "")
        .replace("R", "");
    } else if (item.model_data.fuelSupply.name === "Diesel") {
      monthlyExp =
        (financial.travelling_in_months / 100) *
        item.model_data.fuelCons *
        financial.diesel +
        monthlyInstallment +
        parseInt(financial.insurance);
      monthlyExp = Math.round(monthlyExp);
      monthlyExpFinalValue = currencyFormatter
        .format(monthlyExp, { code: "ZAR" })
        .replace(",00", "")
        .replace("R", "");
    } else {
      monthlyExp =
        (financial.travelling_in_months / 100) *
        item.model_data.fuelCons *
        financial.electric_units +
        monthlyInstallment +
        parseInt(financial.insurance);
      monthlyExp = Math.round(monthlyExp);
      monthlyExpFinalValue = currencyFormatter
        .format(monthlyExp, { code: "ZAR" })
        .replace(",00", "")
        .replace("R", "");
    }

    //console.log(" t"+travellingMnths+" fc"+item.model_data.fuelCons+" d"+diesel+" p"+petrol+" I"+insurance+" mi"+monthlyInstallment)
  }

  let engineCap = item.model_data.engineCap;
  let imageId = item.modelId;
  let enginePos = item.model_data.enginePosition.name;
  let engineConfig = item.model_data.engineConfig.name;
  const s3Image =
    `https://car-glove.s3.us-west-2.amazonaws.com/model/` + imageId;
  let higlight = "";
  if (percentileValues && Object.keys(percentileValues).length > 0) {
    const arrPercentile = Object.keys(percentileValues).map((key) => [
      key,
      percentileValues[key],
    ]);
    if (
      item.sort[0] <= arrPercentile[2][1] &&
      item.sort[0] >= arrPercentile[1][1]
    ) {
      higlight = "green";
    } else if (
      item.sort[0] < arrPercentile[1][1] &&
      item.sort[0] >= arrPercentile[0][1]
    ) {
      higlight = "yellow";
    } else if (item.sort[0] < arrPercentile[0][1]) {
      higlight = "red";
    }
  }
  const rank = item.sort[0];
  const key = item.modelId;
  return {
    key,
    rank,
    s3Image,
    modelName,
    price,
    monthlyInstallmentFinalValue,
    monthlyExpFinalValue,
    engineCap,
    enginePos,
    engineConfig,
    item,
    higlight,
  };
}

function TableRender(props) {
  const {
    data,
    resultStats,
    customFinancial,
    percentileValues,
    analysisName,
    analysisToast,
    financial,
    user
  } = props;

  const dispatch = useDispatch();
  const [openModal, setOpenModal] = React.useState(false);
  const [selectedRow, setSelectedRow] = React.useState({});
  const history = useHistory();
  useEffect(() => {
    if (typeof analysisName === "string" && analysisName) {
      dispatch(showAnalysisToast(true));
    }
  }, []);

  /*console.log(
    "Table d" +
      depositPercent +
      " b" +
      balloonPercent +
      " i" +
      interest +
      " mn" +
      periodMnths
  );*/

  function onhandleClose() {
    setOpenModal(false);
  }

  const dataRows = [];
  data.forEach((item, i) => {
    dataRows.push(
      createData(item, customFinancial, percentileValues, financial)
    );
  });
  console.log(dataRows);

  const handleClose = () => {
    dispatch(fetchScopes());
    dispatch(fetchPreferences());
    dispatch(showAnalysisToast(false));
    history.push("/");
    setTimeout(function () {
      window.location.reload();
    }, 1000);
  };

  if (!dataRows) {
    return <p>Loading</p>;
  }
  const sorter = (a, b) =>
    isNaN(a) && isNaN(b) ? (a || "").localeCompare(b || "") : a - b;

  return (
    <>
      <Typography
        variant="caption"
        display="block"
        gutterBottom
        style={{
          display: "inline-block",
          fontSize: 16,
          color: "#000",
          marginBottom: 20,
          marginTop: 20,
        }}
      >
        Models in selection : {resultStats.numberOfResults}
      </Typography>
      {analysisName && user != null &&(
        <Alert
          onClose={() => handleClose()}
          severity="success"
          color="warning"
          variant="standard"
          className="analysisAlert"
        >
          Analysis Applied: <strong>{analysisName}</strong>
        </Alert>
      )}

      <AntTable
        size="middle"
        pagination={false}
        onRow={(record, rowIndex) => {
          return {
            onDoubleClick: (event) => {
              setSelectedRow(record.item);
              setOpenModal(true);
            },
          };
        }}
        columns={[
          {
            dataIndex: "url",
            key: "url",
            title: "",
            sorting: false,
            render: (text, record) => {
              console.log(record);
              return (
                <Avatar
                  alt="Car Image"
                  src={record.s3Image}
                  style={{
                    margin: "0 auto",
                    width: "60px",
                    height: "unset",
                    borderRadius: "0",
                  }}
                />
              );
            },
          },
          {
            title: "Model",
            dataIndex: "modelName",
            key: "modelName",
            sorter: (a, b) => sorter(a.modelName, b.modelName),
            sortDirections: ["ascend", "descend"],
            render: (text, record) => {
              return {
                props: {
                  style: {
                    color:
                      record.higlight === "green"
                        ? "#63be7b"
                        : record.higlight === "yellow"
                          ? "#F7C003"
                          : record.higlight === "red"
                            ? "#f54254"
                            : "#000",
                    fontWeight: 500,
                  },
                },
                children: (
                  <div
                    style={{
                      textOverflow: "ellipsis",
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      width: 270,
                    }}
                  >
                    <Text tooltip={record.modelName}>{record.modelName}</Text>
                  </div>
                ),
              };
            },
          },
          {
            title: "Price",
            dataIndex: "price",
            key: "price",
            render: (text, record) => {
              console.log(record);

              return {
                children: "R " + record.price,
                props: {
                  style: {
                    width: 200,
                  },
                },
              };
            },
            sorter: (a, b) =>
              sorter(
                a.price.replaceAll(/[ ,]/g, ""),
                b.price.replaceAll(/[ ,]/g, "")
              ),
          },
          {
            title: "Monthly Installment",
            dataIndex: "monthlyInstallmentFinalValue",
            key: "monthlyInstallmentFinalValue",
            render: (text, record) => {
              console.log(record);
              return "R " + record.monthlyInstallmentFinalValue;
            },
            sorter: (a, b) =>
              sorter(
                a.monthlyInstallmentFinalValue.replaceAll(/[ ,]/g, ""),
                b.monthlyInstallmentFinalValue.replaceAll(/[ ,]/g, "")
              ),
          },
          {
            title: "Monthly Expenditure",
            dataIndex: "monthlyExpFinalValue",
            key: "monthlyExpFinalValue",
            render: (text, record) => {
              console.log(record);
              return "R " + record.monthlyExpFinalValue;
            },
            sorter: (a, b) =>
              sorter(
                a.monthlyExpFinalValue.replaceAll(/[ ,]/g, ""),
                b.monthlyExpFinalValue.replaceAll(/[ ,]/g, "")
              ),
          },
          {
            title: "Engine Capacity",
            dataIndex: "engineCap",
            key: "engineCap",
            sorter: (a, b) => sorter(a.engineCap, b.engineCap),
          },
          {
            title: "Engine Position",
            dataIndex: "enginePos",
            key: "enginePos",
            sorter: (a, b) => sorter(a.enginePos, b.enginePos),
          },
          {
            title: "Engine Config",
            dataIndex: "engineConfig",
            key: "engineConfig",
            sorter: (a, b) => sorter(a.engineConfig, b.engineConfig),
          },
        ]}
        expandable={{
          expandIcon: ({ expanded, onExpand, record }) =>
            expanded ? (
              <DownOutlined onClick={(e) => onExpand(record, e)} />
            ) : (
              <RightOutlined onClick={(e) => onExpand(record, e)} />
            ),
          rowExpandable: (record) => record.modelName,
          expandedRowRender: (record) => {
            return (
              <Scrollbars style={{ height: 100 }}>
                <Table
                  size="small"
                  aria-label="purchases"
                  className="collapsibletable"
                >
                  <TableHead>
                    <TableRow>
                      <TableCell
                        align="left"
                        style={{
                          borderLeft: "1px solid #e0e0e0",
                          borderTop: "1px solid #e0e0e0",
                        }}
                      >
                        Fuel Supply
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Power KW
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Power to Mass
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Torque NM
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Gears
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Transmission
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Maximum Speed
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Fuel Consumption
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Air Bags
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        ABS
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Auto Lights
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Cruise control
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Air Conditioner
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        ESP
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ borderTop: "1px solid #e0e0e0" }}
                      >
                        Bluetooth
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{
                          borderTop: "1px solid #e0e0e0",
                          borderRight: "1px solid #e0e0e0",
                        }}
                      >
                        CO2
                      </TableCell>
                    </TableRow>

                    <TableBody>
                      <TableRow>
                        <TableCell
                          align="left"
                          style={{ borderLeft: "1px solid #e0e0e0" }}
                        >
                          {record.item.model_data.fuelSupply.name}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.power_kw}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.powerToMass}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.torque_nm}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.gears}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.transmission}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.maxSpeed}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.fuelCons}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.airbags}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.abs}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.autoLights}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.cruiseControl}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.aircon}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.ESP}
                        </TableCell>
                        <TableCell align="left">
                          {record.item.model_data.bluetooth}
                        </TableCell>
                        <TableCell
                          align="left"
                          style={{ borderRight: "1px solid #e0e0e0" }}
                        >
                          {record.item.model_data.CO2}
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </TableHead>
                </Table>
              </Scrollbars>
            );
          },
        }}
        dataSource={dataRows}
      />

      {openModal && (
        <CardetailModal
          data={selectedRow}
          openModal={openModal}
          handleClose={onhandleClose}
        />
      )}
    </>
  );
}
const Search = (props) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const history = useHistory();
  const token = getToken();
  const params = queryString.parse(location.search);
  console.log(params.analysis);
  let user = JSON.parse(sessionStorage.getItem("user"));
  let selectedAnalysis = params.analysis;
  if (typeof params.analysis === "object") {
    selectedAnalysis =
      params.analysis[
      Object.keys(params.analysis)[Object.keys(params.analysis).length - 1]
      ];
  }

  useEffect(() => {
    if (!token) {
      dispatch(fetchScopes());
      dispatch(fetchPreferences());
      dispatch(showAnalysisToast(false));
      //history.push("/");
    }

    if (token !== null) {
      var name = selectedAnalysis;
      let user = JSON.parse(sessionStorage.getItem("user"));
      axios({
        method: "get",
        url:
          `${process.env.REACT_APP_API_URL}data/getAnalysis?id=` +
          user.id +
          `&name=` +
          name,

        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          console.log(
            "user resp" +
            JSON.stringify(response.data.data.analysisResultSet[0].preference)
          );
          const preferences = JSON.parse(
            response.data.data.analysisResultSet[0].preference
          );
          const defaultFilters = JSON.parse(
            response.data.data.analysisResultSet[0].defaultFilter
          );
          const optionalFilters = JSON.parse(
            response.data.data.analysisResultSet[0].optionalFilter
          );
          dispatch(fetchedDefaultPreferences(preferences));
          dispatch(fetchedDefaultFilters(optionalFilters, defaultFilters));
        })
        .catch((err) => {
          //handle error
          console.log("login error", err);
        });
    } else {
      dispatch(fetchScopes());
      dispatch(fetchPreferences());
      dispatch(showAnalysisToast(false));
      //history.push("/");
    }
    console.log("loading--->");
  }, []);

  const { Preferences, defaultScopes } = props;
  const [completeModalOpen, setCompleteModalOpen] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [selectedFiltersValues, setSelectedFiltersValues] = React.useState([]);
  //console.log("search result value-->" + JSON.stringify(selectedFiltersValues));

  const openCompleteModal = () => {
    
    console.log("user--->" + user);
    if (user === null) {
      console.log("user is not logged in");
      props.toggleSignIn(props.signInModalOpen);
    } else {
      setOpen(true);
    }
    setCompleteModalOpen(true);
  };

  function onHandleCompleteModalClose() {
    //console.log("inside parent close");
    setCompleteModalOpen(false);
  }

  const SEARCH_URL = process.env.REACT_APP_SEARCH_URL;
  const SEARCH_INDEX = process.env.REACT_APP_SEARCH_INDEX;
  const classes = useStylesContainer(props);
  let componentsArr = ["PriceSensor", "InstallmentSensor", "monthly_ExpSensor"];
  if(props.customFinancial){
    componentsArr = [];
  }

  Object.keys(defaultScopes).forEach((index) => {
    const compName = defaultScopes[index]["name"];
    let itemDef = compName.replace(/ /g, "_") + "Sensor";
    //console.log("Default scopes-->" + itemDef);
    componentsArr.push(itemDef);
  });

  function renderSearch() {
    if (Preferences && Object.keys(Preferences).length > 0) {
      let dSource = "";
      let dExpression = "";
      const defaultParams = {};
      let totParams = 0;
      //console.log(Preferences);
      const filteredPreferences = Object.values(Preferences).filter(
        (pref) => pref.visibility === "default"
      );
      //console.log(filteredPreferences);
      Object.keys(filteredPreferences).forEach((index) => {
        const compName = filteredPreferences[index]["name"];
        const ratingField = filteredPreferences[index]["ratingField"];
        //let itemDef = compName.replace(/ /g, "") + "Sensor";
        //componentsArr.push(itemDef);
        const defaultValue = filteredPreferences[index]["defaultValue"];
        const modelField = filteredPreferences[index]["field"];

        defaultParams[compName] = defaultValue;
        if (index == 0) {
          dSource +=
            '((doc["' +
            ratingField +
            '"].value *(params.' +
            compName +
            "))/100)";
          dExpression += '((doc["' + modelField + '"]  == 0 ? 0: 1)';
        } else {
          dSource +=
            "+" +
            '((doc["' +
            ratingField +
            '"].value *(params.' +
            compName +
            "))/100)";
          dExpression += "+" + '(doc["' + modelField + '"]  == 0 ? 0: 1)';
        }
      });

      const defaultSource = dSource;

      totParams = Object.keys(defaultParams).length;
      defaultParams["count"] = totParams;
      //console.log("defaultParams :" + JSON.stringify(defaultParams));

      console.log('financial ' +props.customFinancial);
      console.log('selectedAnalysis ' + selectedAnalysis);
      const defaultQuerySource = () =>{
        let Customsource = "";
        let priceStart,priceEnd,miStart,miEnd,meStart,meEnd;
        if(selectedFiltersValues.PriceSensor && selectedFiltersValues.PriceSensor.value && selectedFiltersValues.InstallmentSensor && selectedFiltersValues.InstallmentSensor.value && selectedFiltersValues.monthly_ExpSensor && selectedFiltersValues.monthly_ExpSensor.value){
          console.log('Query params ' + JSON.stringify(selectedFiltersValues.PriceSensor.value[0]));
           priceStart = JSON.stringify(selectedFiltersValues.PriceSensor.value[0]);
           priceEnd = JSON.stringify(selectedFiltersValues.PriceSensor.value[1]);
           miStart = JSON.stringify(selectedFiltersValues.InstallmentSensor.value[0]);
           miEnd = JSON.stringify(selectedFiltersValues.InstallmentSensor.value[1]);
           meStart = JSON.stringify(selectedFiltersValues.monthly_ExpSensor.value[0]);
           meEnd = JSON.stringify(selectedFiltersValues.monthly_ExpSensor.value[1]);
        }else{
          return <p>Loading</p>
        }
            
        if(props.customFinancial){

          const customQuery =[];
          customQuery.push(`double b=${props.financial.balloon_percent},d=${props.financial.deposit_percent},r=${props.financial.interest} ,n=${props.financial.period_in_months}, i=${props.financial.insurance},t=${props.financial.travelling_in_months};`);
          customQuery.push("double price = doc['model_data.price'].value;");
          customQuery.push("double mi = Math.round((((doc['model_data.price'].value-((doc['model_data.price'].value*d)/100))*(Math.pow((1+(r/1200)),n)) - ((doc['model_data.price'].value*b)/100))*(r/1200))/(Math.pow((1+(r/1200)),n)-1));");
          customQuery.push("double me = doc['model_data.monthlyExp'].value;");
          customQuery.push("if ((doc['model_data.fuelSupply.id'].value) == 800){");
          customQuery.push("me = Math.round(((t/100)*doc['model_data.fuelCons'].value*12.69)+mi+i);");
          customQuery.push("}else if ((doc['model_data.fuelSupply.id'].value) == 801){");
          customQuery.push("me = Math.round(((t/100)*doc['model_data.fuelCons'].value*12.37)+mi+i);");
          customQuery.push("}else if ((doc['model_data.fuelSupply.id'].value) == 802){");
          customQuery.push("me = Math.round(((t/100)*doc['model_data.fuelCons'].value*2.25)+mi+i);");
          customQuery.push("}else if ((doc['model_data.fuelSupply.id'].value) == 803){");
          customQuery.push(" me = Math.round(((t/100)*doc['model_data.fuelCons'].value*2.25)+mi+i);}");
          customQuery.push(`return mi>=${miStart} && mi <= ${miEnd} && me<=${meEnd} && me>=${meStart} && price >= ${priceStart} && price <=${priceEnd};`);
      
          Customsource =  customQuery.join(" "); 
          return {
            sort: {
              _script: {
                type: "number",
                script: {
                  lang: "painless",
                  source: "(" + defaultSource + ")/(params.count)",
                  params: defaultParams,
                },
                order: "desc",
              },
            },
            script_fields: {
              confidence: {
                script: {
                  lang: "expression",
                  source: "(" + dExpression + ")/c)*100",
                  params: {
                    c: totParams,
                  },
                },
              },
            },
            aggs: {
              preferencePercentile: {
                percentiles: {
                  script: {
                    lang: "painless",
                    source: "(" + defaultSource + ")/(params.count)",
                    params: defaultParams,
                  },
                  percents: [100, 75, 50],
                },
              },
            },
            query: {
              bool: {
              must:[ {
                  script: {
                    script: Customsource
                  }
                }]
              }
            },
            _source: true,
          };

        }else{
          return {
            sort: {
              _script: {
                type: "number",
                script: {
                  lang: "painless",
                  source: "(" + defaultSource + ")/(params.count)",
                  params: defaultParams,
                },
                order: "desc",
              },
            },
            script_fields: {
              confidence: {
                script: {
                  lang: "expression",
                  source: "(" + dExpression + ")/c)*100",
                  params: {
                    c: totParams,
                  },
                },
              },
            },
            aggs: {
              preferencePercentile: {
                percentiles: {
                  script: {
                    lang: "painless",
                    source: "(" + defaultSource + ")/(params.count)",
                    params: defaultParams,
                  },
                  percents: [100, 75, 50],
                },
              },
            },
            _source: true,
          };
        }
      }
      return (
        <ReactiveList
          componentId="ResultSensor"
          title="Results"
          from={0}
          size={10}
          showResultStats={false}
          defaultQuery={defaultQuerySource}
          react={{
            and: componentsArr
          }}
          onData={(data) => {
            //console.log(data.rawData.aggregations.preferencePercentile.values);
            dispatch(
              getPercentile(
                data.rawData.aggregations.preferencePercentile.values
              )
            );
            dispatch(setTotal(data.resultStats.numberOfResults));
          }}
          render={({ data, resultStats }) => (
            <>
              <TableRender
                data={data}
                resultStats={resultStats}
                customFinancial={props.customFinancial}
                percentileValues={props.percentileValues}
                analysisName={selectedAnalysis}
                analysisToast={props.analysisToast}
                financial={props.financial}
                user={user}
              />
              {completeModalOpen && (
                <CompleteModal
                  openModal={completeModalOpen}
                  handleClose={onHandleCompleteModalClose}
                  modelItemsData={data.slice(0, 5)}
                  appliedFiltersData={selectedFiltersValues}
                  preferencesData={Preferences}
                  open={open}
                />
              )}
            </>
          )}
          loader={
            <div className={classes.loader}>
              <CircularProgress />
            </div>
          }
        />
      );
    } else {
      return <CircularProgress color="secondary" />;
    }
  }

  //console.log("Components Mounted : " + JSON.stringify(Preferences));
  //console.log("Preferences.length : " + Object.keys(Preferences).length);

  return (
    <div className="content">
      <ReactiveBase
        app={SEARCH_INDEX}
        url={SEARCH_URL}
      // enableAppbase
      >
        <SelectedFilters
          onChange={(selectedValue) => {
            console.log("udhaya-selected-filter:", selectedValue);
            setSelectedFiltersValues(selectedValue);
          }}
          render={() => null}
        />
        <Grid container className="filtercontainer">
          <Grid item xs={12} sm={12} md={3} lg={3} className={classes.spacing}>
            <Sidebar />
          </Grid>
          <Grid item xs={12} sm={12} md={9} lg={9} className={classes.spacing}>
            <div>
              <Preference />
              <PreferenceModal />
              <TableTop />
              <React.Fragment>
                <Typography
                  variant="caption"
                  display="block"
                  gutterBottom
                  style={{
                    display: "inline-block",
                    fontSize: 16,
                    color: "#000",
                    marginBottom: 20,
                    marginTop: 20,
                  }}
                ></Typography>
                <ThemeProvider theme={theme}>
                  <ButtonGroup style={{ float: "right" }}>
                    <Button
                      disabled={props.totalResult > 0 ? false : true}
                      variant="contained"
                      onClick={openCompleteModal}
                      color="primary"
                      size="small"
                      style={{
                        display: "inline-block",
                        marginRight: 10,
                        textTransform: "capitalize",
                        fontSize: 14,
                        marginTop: 20,
                      }}
                    >
                      Complete
                    </Button>
                  </ButtonGroup>
                </ThemeProvider>
              </React.Fragment>
              <div>{renderSearch()}</div>
            </div>
          </Grid>
        </Grid>
      </ReactiveBase>
    </div>
  );
};

const mapStateToProps = (state) => ({
  defaultScopes: state.defaultfilters,
  optionalFilters: state.optionalFilters,
  Preferences: state.defaultPreferences,
  customFinancial: state.customFinancial,
  interest: state.interest,
  balloonPercent: state.balloonPercent,
  depositPercent: state.depositPercent,
  periodMnths: state.periodMnths,
  diesel: state.diesel,
  petrol: state.petrol,
  travellingMnths: state.travellingMnths,
  insurance: state.insurance,
  percentileValues: state.percentileValues,
  elecUnits: state.elecUnits,
  analysisToast: state.analysisToast,
  financial: state.financial,
  totalResult: state.totalResult,
});
const mapDispatchToProps = (dispatch) => ({
  toggleSignIn: (value) => dispatch(toggleSignIn(value)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(useStyles)(Search));
