import React, { useState } from "react";
import { Page, Text, View, Document, StyleSheet } from "@react-pdf/renderer";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import PrintableContent from "../carDetail/PrintableContent";
// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
});

// Create Document Component
const MyDocument = (props) => (
  <Document>
    <Page size="A4" style={styles.page}>
      <View style={styles.section}>
        <Text>
          {props.modelItems.map((item) => (
            <Grid container>
              <Grid item xs={12} sm={12} md={6} lg={6}>
                <Typography
                  style={{
                    fontSize: 21,
                    marginTop: 20,
                    color: "#EA001E",
                    paddingLeft: 16,
                    fontWeight: "bold",
                  }}
                >
                  {item.model_data.manufacturer.name}
                  {" " + item.model_data.modelName}
                </Typography>
                <Typography
                  style={{
                    fontSize: 18,
                    marginTop: 20,
                    color: "#9c1233",
                    paddingLeft: 16,
                  }}
                >
                  Key Specification
                </Typography>
                <PrintableContent data={item} />
              </Grid>
            </Grid>
          ))}
        </Text>
      </View>
      <View style={styles.section}>
        <Text>Section #2</Text>
      </View>
    </Page>
  </Document>
);

export default MyDocument;
