//files-import
import Header from "../includes/Header";
import Footer from "../includes/Footer";
import React from "react";
import Search from "./Search";

const Home = (props) => {
  const token = props.location.search;
  console.log("token===" + token);

  return (
    <div className="content bodyContent">
      <Header data={token} />
      <Search />
      <Footer />
    </div>
  );
};

export default Home;
