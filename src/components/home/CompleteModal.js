import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import CarKeyspecificationcol1 from "../carDetail/CarKeyspecificationcol1";
import CarKeyspecificationcol2 from "../carDetail/CarKeyspecificationcol2";
import CarKeyfeaturescol1 from "../carDetail/CarKeyfeaturescol1";
import CarKeyfeaturescol2 from "../carDetail/CarKeyfeaturescol2";
import Pagination from "./Pagination";
import "../../App.css";
import MailIcon from '@material-ui/icons/Mail';
import moment from "moment";
import Tooltip from '@material-ui/core/Tooltip';
import axios from "axios";
import { getToken } from "../Utils/Common";
import { Alert, AlertTitle } from "@material-ui/lab";
const styles = (theme) => ({
  ///react-pdf styles
  page: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    top: -5,
    color: "#ffffff",
    right: -5,
    padding: 0,
    position: "absolute",
    background: "#dc3545",
    width: 17,
    height: 17,
    "&:hover": {
      background: "#dc3545",
    },
  },
  title: {
    margin: 0,
    padding: theme.spacing(2),
    position: "relative",
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.title} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon fontSize="small" />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const CompleteModal = (props) => {
  const open = props.open;
  const [downloadLink, setDownloadLink] = useState(false);
  const [modelItems, setModelItems] = useState([]);
  const [sentMail, setsentMail] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(1);
  const [successful, setSuccessful] = useState();
  const [financialData, setFinancialData] = useState({});

  const setData = () => {
    setModelItems(props.modelItemsData);
    setDownloadLink(true);

    let tempFinance = { ...props.financial };
    tempFinance.period_in_years = (tempFinance.period_in_months / 12).toFixed(
      1
    );
    tempFinance.travelling_in_years = tempFinance.travelling_in_months * 12;
    delete tempFinance.travelling_in_months;
    delete tempFinance.period_in_months;
    setFinancialData(tempFinance);

    delete props.appliedFiltersData.analysis;
    Object.keys(props.appliedFiltersData).map((key, i) => {
      if (props.appliedFiltersData[key].value === null) {
        delete props.appliedFiltersData[key];
      }
    });
  };
  //useEffect(setData, handleOpen());

  useEffect(() => {
    setData();
  }, []);

  console.log("udhaya-filters:", props.appliedFiltersData);
  console.log("udhaya-preference:", props.preferencesData);
  console.log("udhaya-financial:", financialData);
  console.log("udhaya-financial-pp:", props.financial);

  // Get current posts
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = modelItems.slice(indexOfFirstPost, indexOfLastPost);
  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  //const currentData = data[0];

  const [fullWidth] = React.useState(true);
  const [maxWidth] = React.useState("md");

  const fileName =
    "carglove-report-" + moment().format("MMDDYYYY-HHmm") + ".pdf";

  console.log("udhaya-moment", moment().format("MMMM Do YYYY, h:mm:ss a"));
  const sendPDF = () =>{
    console.log(props.appliedFiltersData);
    let user = JSON.parse(sessionStorage.getItem("user"));
    let token = getToken();
    axios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}data/completePdf`,
      data: {
        userId: user.id,
        modelItems:modelItems,
        appliedFilters: props.appliedFiltersData,
        preferencesData: props.preferencesData,
        financialData: financialData
      },
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        console.log("Send PDF response: ", response);
        if (response.status === 200 || response.status === 201) {
          console.log("data saved");
          setSuccessful(true);
          setsentMail(true);
          setTimeout(() => {
            setsentMail(false);
          }, 3000);
        }else{
          setSuccessful(false);
          setTimeout(() => {
            setSuccessful();
          }, 3000);
        }
      })
      .catch((err) => {
        //handle error
        console.log("login error", err);
      });
  }

  const renderMessage = () =>{
    //alert(successful)
    if(sentMail && successful){
      return(
      <Grid container justify = "center">
                  <Alert severity="success" >
            <AlertTitle> Email Sent Successfully...</AlertTitle>
          </Alert>
      </Grid>
      )
    }else if(successful === false){
      return(
        <Grid container justify = "center">
          <Alert severity="error">
            <AlertTitle> Something wrong with sending email,Please try again later...</AlertTitle>
          </Alert>
      </Grid>
      )
    }
  }

  return (
    <div>
      {open && (
        <Dialog
          onClose={props.handleClose}
          aria-labelledby="customized-dialog-title"
          open={props.openModal}
          fullWidth={fullWidth}
          maxWidth={maxWidth}
        >
          <DialogTitle
            id="customized-dialog-title"
            onClose={props.handleClose}
            style={{ textAlign: "center", fontSize: 18, color: "#4f4f4f" }}
          >
            Complete Modal
          </DialogTitle>
          
          <Tooltip title="Send Email">
            <IconButton aria-label="close" style={{position: "absolute",right: 0,top: "7%",marginRight: "25px"}} onClick={()=> sendPDF(props)}>
                <MailIcon fontSize="medium" />
            </IconButton>
          </Tooltip>

          {renderMessage()}

          <DialogContent>
            {" "}
            {currentPosts.map((item) => (
              <Grid container>
                <Typography
                  style={{
                    fontSize: 21,
                    marginTop: 20,
                    color: "#EA001E",
                    paddingLeft: 16,
                    fontWeight: "bold",
                    display: "block",
                    width: "100%",
                  }}
                >
                  {item.model_data.manufacturer.name}
                  {" " + item.model_data.modelName}
                </Typography>

                <Typography
                  style={{
                    fontSize: 18,
                    marginTop: 20,
                    color: "#9c1233",
                    paddingLeft: 16,
                    display: "block",
                    width: "100%",
                  }}
                >
                  Key Specifications
                </Typography>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <CarKeyspecificationcol1 data={item} />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <CarKeyspecificationcol2 data={item} />
                </Grid>
                <Typography
                  style={{
                    fontSize: 18,
                    marginTop: 20,
                    color: "#9c1233",
                    paddingLeft: 16,
                    display: "block",
                    width: "100%",
                  }}
                >
                  Key Features
                </Typography>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <CarKeyfeaturescol1 data={item} />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <CarKeyfeaturescol2 data={item} />
                </Grid>
              </Grid>
            ))}
          </DialogContent>
          <Pagination
            postsPerPage={postsPerPage}
            totalPosts={modelItems.length}
            paginate={paginate}
          />
          <DialogActions>
            <Button
              autoFocus
              onClick={props.handleClose}
              style={{ color: "#9c1233" }}
            >
              Close
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    signInModalOpen: state.signInModalOpen,
    financial: state.financial,
  };
};

export default connect(mapStateToProps, null)(CompleteModal);
