import React from "react";
import { withStyles } from "@material-ui/core/styles";

const useStyles = (theme) => ({
  paperContainer: {
    height: 281,
    backgroundImage: `url(${
      process.env.PUBLIC_URL + "/img/PreferenceManual3.png"
    })`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    position: "relative",
    backgroundPosition: "center",
  },

  accorheading: {
    left: 0,
    position: "absolute",
    textAlign: "center",
    top: "30%",
    width: "100%",
    fontSize: 48,
    color: "#fff",
  },
});

class Banner extends React.Component {
  render() {
    const { classes } = this.props;
    return <div className={classes.paperContainer}></div>;
  }
}
export default withStyles(useStyles)(Banner);
