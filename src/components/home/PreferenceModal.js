import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Link from "@material-ui/core/Link";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Banner from  "./Banner.js"

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    top: -5,
    color: "#ffffff",
    right: -5,
    padding: 0,
    position: "absolute",
    background: "#dc3545",
    width: 17,
    height: 17,
    "&:hover": {
      background: "#dc3545",
    },
  },
  title: {
    margin: 0,
    padding: theme.spacing(2),
    position: "relative",
  },
});

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));



const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.title} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon fontSize="small" />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function SimpleModal() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const [fullWidth] = React.useState(true);
  const [maxWidth] = React.useState("md");

  const body = (
    <Dialog
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      open={handleOpen}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
    >
      <DialogTitle
        id="customized-dialog-title"
        onClose={handleClose}
        style={{ textAlign: "center", fontSize: 18, color: "#4f4f4f" }}
      >
        How to use preference
      </DialogTitle>
      <DialogContent>
        <Grid container>
          <Grid item xs={12} sm={12} md={12} lg={12}>
           
            <Banner />
            <Typography
              style={{
                fontSize: 18,
                marginTop: 20,
                color: "#9c1233",
                paddingLeft: 16,
                display: "block",
                width: "100%",
              }}
            >
              Preference Features :
            </Typography>
            <ListItem>
                <ListItemText primary="1. Preference Sections (section 1 and section 2)" />
              </ListItem>
              <ListItem>
                <ListItemText
                  primary="2. Section 1(Basic items)"
                />
              </ListItem>
              <ListItem>
                <ListItemText primary="3. Section 2(Advanced items in list)" />
              </ListItem>
              <ListItem>
                <ListItemText
                  primary="4. Range Slider with Handle"
                />
              </ListItem>
              <ListItem>
                <ListItemText
                  primary="5. Tooltip"
                />
              </ListItem>
              <ListItem>
                <ListItemText
                  primary="6. Close Button"
                />
              </ListItem>

             <Typography
              style={{
                fontSize: 18,
                marginTop: 20,
                color: "#9c1233",
                paddingLeft: 16,
                display: "block",
                width: "100%",
              }}
            >
              How Preference Works?
            </Typography>

            <Typography
              gutterBottom
              style={{
                fontSize: 14,
                color: "#4f4f4f",
                marginLeft: 16,
              }}
            >
             <b>1. Preference sections</b> :  Preference has two sections (Section 1 and Section 2), By default all the items will be set to 50 in section 1. Section 1 has basic items whereas Section 2 has a list of advanced items in it.
            </Typography>

             <Typography
              gutterBottom
              style={{
                fontSize: 14,
                color: "#4f4f4f",
                marginLeft: 16,
              }}
            >
           <b>2. Add items to section 1</b> : As a user you can add items to section 1, by just dragging and dropping the items from the list of items under section 2.
            </Typography>

             <Typography
              gutterBottom
              style={{
                fontSize: 14,
                color: "#4f4f4f",
                marginLeft: 16,
              }}
            >
             <b>3. Know more about the item</b> : Hover over each item will open a tooltip where you can find the detailed information about the item.
            </Typography>

             <Typography
              gutterBottom
              style={{
                fontSize: 14,
                color: "#4f4f4f",
                marginLeft: 16,
              }}
            >
           <b> 4. Change values in slider</b> : If you wish to change the default value of items in section 1. Adjust the slider up and down using the handle, based on the selected value some rank calculation will be done and the search results will be sorted accordingly.
            </Typography>

             <Typography
              gutterBottom
              style={{
                fontSize: 14,
                color: "#4f4f4f",
                marginLeft: 16,
              }}
            >
            <b>5. Know the changed value</b> : On sliding the range slider, the selected value will be shown in the badges.
            </Typography>

             <Typography
              gutterBottom
              style={{
                fontSize: 14,
                color: "#4f4f4f",
                marginLeft: 16,
              }}
            >
           <b>6. Add items back to section 2</b> : If you wish to remove any item from section 1, Click close icon in the item to add the item back to section 2.
            </Typography>

          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleClose} style={{ color: "#9c1233" }}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );

  return (
    <div>
      <Link component="button" onClick={handleOpen} className={classes.linktxt}>
        How to use preference
      </Link>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
