//react-import
import React, { useEffect ,useState} from "react";
//styles-import
import { makeStyles } from "@material-ui/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";
import MoreOptionItemsList from "./MoreOptionItemsList";
import { connect } from "react-redux";
import { useDispatch } from "react-redux";
import {
  setMonthlyExpenditure,
  fetchScopes,
  setMaxPrice,
  fetchPreferences,
  showAnalysisToast,
} from "../../actions/actionCreators";
import { useHistory } from "react-router-dom";
import { RangeInput, SelectedFilters } from "@appbaseio/reactivesearch";
import DefaultComponents from "./DefaultComponents";
import currencyFormatter from "currency-formatter";

//theme
//styles
const useStyles = makeStyles(() => ({
  poppverbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
    marginTop: 20,
  },

  iconpoppverbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: 45,
    color: "#fff",
  },

  textbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },

  onchangetextbtns: {
    textTransform: "capitalize",
    // backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    // borderColor: "#9c1233",
    backgroundColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
      backgroundColor: "#9c1233",
    },
  },

  moretoptionbtn: {
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    color: "#fff",
    marginTop: 20,
    height: 60,
    borderRadius: 4,
    maxWidth: "100%",
    width: "100%",
  },

  closebtn: {
    width: 15,
    height: 15,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
  },

  accorheading: {
    fontSize: 14,
    fontWeight: 500,
  },
}));

const Sidebar = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [showMoreButtons, setMoreButtons] = React.useState(false);
  const [showMoreLabel, setMoreLabel] = React.useState("More Options");
  const moreonClick = () => {
    setMoreButtons(!showMoreButtons);
    if (showMoreLabel === "More Options") setMoreLabel("Hide Options");
    else setMoreLabel("More Options");
  };

  const dispatch = useDispatch();
  const maxPrice = props.maxPrice;
const [price, setPrice] = useState([]);
const [monthlyIns, setMonthlyIns] = useState([]);
const [monthlyExp, setMonthlyExp] = useState([])
  const [loading, setLoading] = React.useState(true);

  useEffect(() => {
    fetchData();
    if (Object.keys(props.defaultScopes).length === 0) {
      props.getScopes();
    }

    console.log("loading--->" + loading);
  }, []);

  const handlePriceMax = (e) => {
    //console.log(e.end);
    //const val = e.end;
    props.setMaxPrice(e.end);
    props.setMonthlyExpenditure();
  };

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}data/financial`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((response) => {
        console.log("priceresult" + response.data);
        let fData = response.data.financialResultSet[0];
        setPrice([fData.minPrice,fData.maxPrice]);
       
        if (
          !sessionStorage.getItem("financialData") &&
          !sessionStorage.getItem("user")
        )
        {
        props.setMaxPrice(fData.maxPrice);
        props.setMonthlyExpenditure();
        } 
        const insMinValue = Math.round(((( fData.minPrice- (fData.minPrice * fData.depositInPercent) / 100) *Math.pow(1 + fData.interestRate / 1200, fData.periodInMnths) -
        (fData.minPrice* fData.balloonInPercent) / 100) *
        (fData.interestRate / 1200)) /
      (Math.pow(1 + fData.interestRate / 1200, fData.periodInMnths) - 1));
        const insMaxValue = Math.round(((( fData.maxPrice- (fData.maxPrice * fData.depositInPercent) / 100) *Math.pow(1 + fData.interestRate / 1200, fData.periodInMnths) -
        (fData.maxPrice* fData.balloonInPercent) / 100) *
        (fData.interestRate / 1200)) /
      (Math.pow(1 + fData.interestRate / 1200, fData.periodInMnths) - 1));
      setMonthlyIns([insMinValue,insMaxValue]);

      const expMinValue=Math.round((fData.travellingpm / 100) *fData.fuelConsMin *fData.electUnits +insMinValue +fData.insurance);
      const expMaxValue=Math.round((fData.travellingpm / 100) *fData.fuelConsMax *fData.petrol95 +insMaxValue +fData.insurance);
      setMonthlyExp([expMinValue,expMaxValue]);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  };

  const clearFilter = (clearFilter) => {
    console.log("clear " + clearFilter);
    dispatch(fetchScopes());
    dispatch(fetchPreferences());
    dispatch(showAnalysisToast(false));
    history.push("/");
    setTimeout(function () {
      window.location.reload();
    }, 1000);

    return clearFilter;
  };

  if(loading){
    return <p>Loading</p>
  }
  
  return (
    <div>
      <Card style={{ borderRadius: 0 }}>
        <SelectedFilters
          onChange={(selectedValue) => {
            console.log(selectedValue);
          }}
          render={({ clearValues }) => (
            <ButtonGroup style={{ float: "right" }}>
              <Button
                variant="contained"
                color="primary"
                size="small"
                style={{
                  marginRight: 10,
                  textTransform: "capitalize",
                  fontSize: 14,
                  marginTop: 10,
                }}
                onClick={() => clearFilter(clearValues)}
              >
                Reset
              </Button>
            </ButtonGroup>
          )}
        />
        <CardContent style={{ display: "inline-block" }}>
          <Accordion defaultExpanded className={useStyles.sidebaraccordion}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={useStyles.accorheading}>Price</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <Grid container spacing={2}>
                    <RangeInput
                      dataField="model_data.price"
                      componentId="PriceSensor"
                      tooltipTrigger="hover"
                      onValueChange={handlePriceMax}
                      URLParams
                      range={{
                        start: Number(price[0]),
                        end:  Number(price[1]),
                      }}
                      renderTooltipData={data => {
                        let tipValue =  currencyFormatter
                        .format(data, { code: "ZAR" })
                        .replace(",00", "")
                        .replace("R", "R ");
                        ;
                        return(
                            <>
                            {tipValue}
                            </>
                      )}}
                      stepValue={1000}
                      showHistogram={true}
                      className="custom-range-slider"
                      innerClass={{
                        slider: "custom-range-slider-bar",
                        "input-container": "custom-range_input-container",
                        input: "custom-range_input-field"
                      }}
                    />
                </Grid>
              </Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion style={{ marginBottom: 0, marginTop: 0 }}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography className={useStyles.accorheading}>
                Monthly Installment
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <Grid container spacing={2}>
                  <RangeInput
                    dataField="model_data.monthlyInstallment"
                    componentId="InstallmentSensor"
                    tooltipTrigger="hover"
                    URLParams={true}
                    range={{
                      start: Number(monthlyIns[0]),
                      end: props.customFinancial?Math.round(props.monthlyInstallment):Number(monthlyIns[1]),
                    }}
                    renderTooltipData={data => {
                      let tipValue =  currencyFormatter
                      .format(data, { code: "ZAR" })
                      .replace(",00", "")                        
                      .replace("R", "R ");
                      return(
                          <>
                          {tipValue}
                          </>
                    )}}
                    stepValue={1000}
                    showHistogram={false}
                    className="custom-range-slider"
                    innerClass={{
                      slider: "custom-range-slider-bar",
                      "input-container": "custom-range_input-container",
                      input: "custom-range_input-field"
                    }}
                  />
                </Grid>
              </Typography>
            </AccordionDetails>
          </Accordion>

          <Accordion style={{ marginBottom: 0, marginTop: 0 }}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography className={useStyles.accorheading}>
                Monthly Expenditure
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <Grid container spacing={2}>
                  <RangeInput
                    dataField="model_data.monthlyExp"
                    componentId="monthly_ExpSensor"
                    tooltipTrigger="hover"
                    URLParams
                    range={{
                      start: Number(monthlyExp[0]),
                      end: props.customFinancial?Math.round(props.monthlyExpenditure):Number(monthlyExp[1])
                    }}
                    stepValue={1000}
                    renderTooltipData={data => {
                      let tipValue =  currencyFormatter
                      .format(data, { code: "ZAR" })
                      .replace(",00", "")
                      .replace("R", "R ");
                      return(
                          <>
                          {tipValue}
                          </>
                    )}}
                    showHistogram={false}
                    className="custom-range-slider"
                    innerClass={{
                      slider: "custom-range-slider-bar",
                      "input-container": "custom-range_input-container",
                      input: "custom-range_input-field"
                    }}
                  />
                </Grid>
              </Typography>
            </AccordionDetails>
          </Accordion>
        </CardContent>
      </Card>
      
      <DefaultComponents />
      <MoreOptionItemsList />
     
     {/*  <ButtonGroup
        variant="contained"
        className={classes.poppverbtns}
        aria-label="split button"
        style={{ marginTop: 20 }}
      >
        <Button
          aria-describedby="country-popover"
          variant="contained"
          className={classes.textbtns}
          size="large"
          onClick={moreonClick}
        >
          {showMoreLabel} - ({Object.keys(props.optionalFilters).length})
        </Button>
      </ButtonGroup>
      {showMoreButtons ? <MoreOptionItemsList /> : null} */}
    </div>
  );
};

const mapStateToProps = (state) => ({
  defaultScopes: state.defaultfilters,
  optionalFilters: state.optionalFilters,
  loading: state.loading,
  maxPrice: state.maxPrice,
  customFinancial:state.customFinancial,
  monthlyInstallment:state.financial.monthlyIns,
  monthlyExpenditure:state.financial.monthlyExp,
  interest: state.interest,
  balloonPercent: state.balloonPercent,
  depositPercent: state.depositPercent,
  periodMnths: state.periodMnths,
  diesel: state.diesel,
  petrol: state.petrol,
  travellingMnths: state.travellingMnths,
  insurance: state.insurance,
});

const mapDispatchToProps = (dispatch) => ({
  getScopes: () => dispatch(fetchScopes()),
  setMaxPrice: (value) => dispatch(setMaxPrice(value)),
  setMonthlyExpenditure: () => dispatch(setMonthlyExpenditure()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
