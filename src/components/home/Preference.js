import React, { useState, useEffect } from "react";
import DragItem from "../drag-item";
import DropItem from "../drop-item";
import DefaultView from '../drag-item/defaultView';
import "../../App.css";
import Grid from "@material-ui/core/Grid";
import { Scrollbars } from "react-custom-scrollbars";
import { fetchPreferences, adddefaultPreferences } from '../../actions/actionCreators';
import { connect } from "react-redux";
const Preferences = (props) => {
  //const [carValues, setCarValue] = useState([]);
  const [today] = useState(new Date());
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = () => {
    setLoading(true);
    
    if(Object.keys(props.Preferences).length === 0){
      props.getPreferences();
    }
  };
  
  const PreferenceValues = props.Preferences;
  console.log('defpre :' + JSON.stringify(PreferenceValues));
  const scrollContainerStyle = {
    width: "auto",
    height: "295px",
    overflow: "auto",
    whiteSpace: "nowrap",
    background: "#dfe3e6",
  };

  return (
    <div className="App">
      <div className="box">
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={9} lg={10}>
            <div className="Preferenceside1">
              <div
                className="scrollbar scrollbar-primary mx-auto"
                style={scrollContainerStyle}
              >
                <DropItem
                  heading="Preference"
                  onDrop={(id) => {
                    const currentCar = { ...PreferenceValues[id - 1] };
                    console.log(PreferenceValues[id]);
                    props.addtoDefault(currentCar);
                  }}
                >               
                  {Object.keys(PreferenceValues)
                    .map((key) => ({ id: key, ...PreferenceValues[key] }))
                    .filter((car) => car.visibility === "default")
                    .map((car) => (
                      <DefaultView  data={car} allPreferences={props.Preferences} />
                    ))}
               </DropItem> 
              </div>
            </div>
          </Grid>

          <Grid item xs={12} sm={12} md={3} lg={2}>
            <div className="Preferenceside2">
              <Scrollbars style={{ height: 295 }}>
                <DropItem
                  heading="PreferenceList"
                  onDrop={(id) => {
                    const currentCar = { ...PreferenceValues[id] };
                    console.log(currentCar);
                    //props.addtoDefault(currentCar);
                  }}
                >
                  {Object.keys(PreferenceValues)
                    .map((key) => ({ id: key, ...PreferenceValues[key] }))
                    .filter((car) => car.visibility === "optional")
                    .map((car, index) => (
                      <DragItem id={car.seqNo} data={car} key={car.seqNo} />
                    ))}
                </DropItem>
              </Scrollbars>
            </div>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  Preferences : state.defaultPreferences,
  defaultPreferenceValue: state.defaultPreferenceValue,
  loading: state.loading
})

const mapDispatchToProps = dispatch => ({
  getPreferences : () => dispatch(fetchPreferences()),
  addtoDefault: value => dispatch(adddefaultPreferences(value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Preferences);
