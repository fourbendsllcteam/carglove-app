import React from "react";
import { makeStyles } from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import MuseumIcon from "@material-ui/icons/Museum";
import Chip from "@material-ui/core/Chip";
import "antd/dist/antd.css";
import { connect } from "react-redux";
import {
  remove_default_filters,
  addmoreFilter,
} from "../../actions/actionCreators";
import { MultiDropdownList, DynamicRangeSlider } from "@appbaseio/reactivesearch";
import Tooltip from '@material-ui/core/Tooltip';
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
const useStyles = makeStyles(() => ({
  poppverbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
    marginTop: 20,
  },

  iconpoppverbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    minWidth: "45px !important",
    color: "#fff",
    zIndex: 1,
  },

  closepoppverbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: 45,
    color: "#fff",
    padding: "2px !important",
  },

  textbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },

  onchangetextbtns: {
    textTransform: "capitalize",
    // backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    // borderColor: "#9c1233",
    backgroundColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
      backgroundColor: "#9c1233",
    },
  },

  moretoptionbtn: {
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    color: "#fff",
    marginTop: 20,
    height: 60,
    borderRadius: 4,
    maxWidth: "100%",
    width: "100%",
  },
  accorheading: {
    fontSize: 14,
    fontWeight: 500,
  },
}));


function DefaultComponents(props) {
  const classes = useStyles();
  const scopeValues = props.defaultScopes;
  console.log("default :" + JSON.stringify(scopeValues));

  const handleClick = (scope) => {
    console.log(scope);
    props.removeDefault(scope.id);
    props.addtoMore(scope);
  };

  const captilizeAllWords = (sentence) => {
    if (typeof sentence !== "string") return sentence;
    return sentence.split(' ')
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }

  const componentsArr = ["PriceSensor","InstallmentSensor","monthly_ExpSensor"];
  Object.keys(props.defaultScopes).forEach((index) => {
    const compName = props.defaultScopes[index]["name"];
    let itemDef = compName.replace(/ /g, "_") + "Sensor";
    componentsArr.push(itemDef);
  });

  const removeCurrentSensor = (allSensor, current) => {
    let removed = allSensor.filter((item) => item !== current);
    return removed;
  };
  console.log("Jothiiii " + componentsArr);
  return (
    <div>
      {Object.keys(scopeValues)
        .map((key) => ({ id: key, ...scopeValues[key] }))
        .filter((scope) => scope.visibility === "default" && scope.filterType === "multi-select-filter")
        .sort((a, b) => (a.seqNo > b.seqNo ? 1 : -1))
        .map((scope) => (
          <div
            key={"default_" + scope.id}
            style={{ position: "relative", marginTop: 20, marginRight:13 }}
          >
            <Button className="multilistIconpoppverbtns">
              <MuseumIcon />
            </Button>
            <MultiDropdownList
              className="MultiListDropdown"
              componentId={scope.name.replace(/ /g, "_") + "Sensor"}
              dataField={scope.field}
              selectAllLabel="Select All"
              showSearch={false}
              URLParams={true}
              react={{
                and: removeCurrentSensor(
                  componentsArr,
                  scope.name.replace(/ /g, "_") + "Sensor"
                ),
              }}
              placeholder={captilizeAllWords(scope.name)}
              showCheckbox={true}
              renderNoResults={() => <div className="MultilistZeroResult"> 
              <Tooltip title={"No "+captilizeAllWords(scope.name) +" scope available for the current selection"} 
              placement="top">
                <p>{captilizeAllWords(scope.name)} (0)</p></Tooltip>
             </div>
             }
            />
            <Chip
              label="x"
              className="MultilistCloseBtn"
              onClick={() => handleClick(scope)}
            />
          </div>
        ))}

      {Object.keys(scopeValues)
        .map((key) => ({ id: key, ...scopeValues[key] }))
        .filter((scope) => scope.visibility === "default" && scope.filterType === "range-filter")
        .sort((a, b) => (a.seqNo > b.seqNo ? 1 : -1))
        .map((scope) => (
          <div className="sidebarScopeFilter"
            key={"default_" + scope.id}
            style={{  marginTop: 20, width: "95%", position: "relative" }}
          >
          <Accordion defaultExpanded className={useStyles.sidebaraccordion}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={useStyles.accorheading}>{scope.name}</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>

            <DynamicRangeSlider
            componentId={scope.name.replace(/ /g, "_") + "Sensor"}
            dataField={scope.field}
            URLParams={true}
            tooltipTrigger="hover"
            react={{
              and: removeCurrentSensor(
                componentsArr,
                scope.name.replace(/ /g, "_") + "Sensor"
              ),
            }}
            showHistogram={false}
            stepValue={1}
            className="custom-range-slider"
            innerClass={{
              slider: "custom-range-slider-bar",
            }}
          />
           
          </Typography>
          </AccordionDetails>
          
          </Accordion>
           <Chip
              label="x"
              className="MultilistCloseBtn-scopeFilter"
              onClick={() => handleClick(scope)}
            />
          </div>
      ))}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    defaultScopes: state.defaultfilters,
  };
};

const mapDispatchToProps = (dispatch) => ({
  removeDefault: (value) => dispatch(remove_default_filters(value)),
  addtoMore: (value) => dispatch(addmoreFilter(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DefaultComponents);
