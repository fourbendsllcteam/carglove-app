import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";



const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function PrintableContent(props) {
  console.log("car specs-->" + props.data.brakes);

  const classes = useStyles();
  const data = props.data.model_data;
  return (
    <div className={classes.root}>
      <List>
        <ListItem>
          <ListItemText primary="Door" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.doors} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Seats" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.seats} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />

        <ListItem>
          <ListItemText primary="Parts List" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.partslist} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />

        <ListItem>
          <ListItemText primary="Engine Comp Ratio" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.engineCompRatio} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Fuel Supl Details" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.fuelSuplDetails} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Torque_rpm" />
          <ListItemSecondaryAction>
            <ListItemText primary={props.data.torque_rpm} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Engine Speed120" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.engineSpeed120} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Transm Details" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.transmDetails} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="AccelDetails" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.accelDetails} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Fuel Capacity" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.fuelCapacity} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="OTaccelDetails" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.OTaccelDetails} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="SpeedCalibration" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.speedCalibration} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="fuelDetails" />
          <ListItemSecondaryAction>
            <ListItemText primary={data.fuelDetails} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
      </List>
    </div>
  );
}
