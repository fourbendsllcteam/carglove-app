import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import currencyFormatter from "currency-formatter";
import Text from "react-texty";
import "react-texty/styles.css";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function CarKeyspecificationcol2(props) {
  const classes = useStyles();
  const data = props.data.model_data;
  return (
    <div className={classes.root}>
          <List>
            <ListItem>
              <ListItemText primary="Price" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={ currencyFormatter
      .format(data.price, { code: "ZAR" })
      .replace(",00", "").replace("R", "R ")} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Gears" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.gears}>{data.gears}</Text>} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Brakes" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.brakes}>{data.brakes}</Text>} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Engine Bore/Stroke" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs"  primary={ <Text tooltip={data.engineBoreStroke}>{data.engineBoreStroke}</Text>} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            
            <ListItem>
              <ListItemText primary="Valve Gear" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.valveGear}>{data.valveGear}</Text>} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Power_rpm" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.power_rpm}>{data.power_rpm}</Text>} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
             <ListItem>
              <ListItemText primary="Wheels Tyres" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.wheelsTyres}>{data.wheelsTyres}</Text>} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
             <ListItem>
              <ListItemText primary="Steering Details" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.steeringDetails}>{data.steeringDetails}</Text>} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
             <ListItem>
              <ListItemText primary="Suspension" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.suspension}>{data.suspension}</Text>} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
             <ListItem>
              <ListItemText primary="Warranty" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.warranty}>{data.warranty}</Text>} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
             <ListItem>
              <ListItemText primary="Vehicle Dimensions" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary= {<Text tooltip={data.vehicleDimensions}>{data.vehicleDimensions}</Text>}/>
              </ListItemSecondaryAction>
            </ListItem>
            <Divider /> 
            <ListItem>
              <ListItemText primary="Features List" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={data.featuresList} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider /> 
                    
          </List>
    </div>
  );
}
