import React from "react";
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";



export default function Cardetailgallery(props) {
    const s3ImageId=props.data;
    console.log("S3 Image ID===>"+s3ImageId);
        const images = [{
        original: "https://car-glove.s3.us-west-2.amazonaws.com/model/"+s3ImageId,
        thumbnail: "https://car-glove.s3.us-west-2.amazonaws.com/model/"+s3ImageId,
    },
    {
        original: "https://car-glove.s3.us-west-2.amazonaws.com/model/"+s3ImageId,
        thumbnail: "https://car-glove.s3.us-west-2.amazonaws.com/model/"+s3ImageId,
    },
];

    return(
 <ImageGallery items = { images }
        showPlayButton={false}
        thumbnailPosition="left"
        showFullscreenButton={false}
        showNav={false}
        />
    ); 
}
