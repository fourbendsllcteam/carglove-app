import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Text from "react-texty";
import "react-texty/styles.css";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function CarKeyspecificationcol1(props) {
  console.log("car specs-->" + props.data.brakes);

  const classes = useStyles();
  const data = props.data.model_data;
  return (
    <div className={classes.root}>
      <List>
        <ListItem>
          <ListItemText primary="Doors" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={<Text tooltip={data.doors}>{data.doors}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Seats" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={<Text tooltip={data.seats}>{data.seats}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />

        <ListItem>
          <ListItemText primary="Parts List" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.partsList}>{data.partsList}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />

        <ListItem>
          <ListItemText primary="Engine Compression Ratio" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.engineCompRatio}>{data.engineCompRatio}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Fuel Supply Details" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.fuelSuplDetails}>{data.fuelSuplDetails}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Torque_rpm" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.torque_rpm}>{data.torque_rpm}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Engine Speed120" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.engineSpeed120}>{data.engineSpeed120}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Transmission Details" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.transmDetails}>{data.transmDetails}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Acceleration Details" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.accelDetails}>{data.accelDetails}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Fuel Capacity" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={ <Text tooltip={data.fuelCapacity}>{data.fuelCapacity}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="OTaccel Details" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={<Text tooltip={data.OTaccelDetails}>{data.OTaccelDetails}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Speed Calibration" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={<Text tooltip={data.speedCalibration}>{data.speedCalibration}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Fuel Details" />
          <ListItemSecondaryAction>
            <ListItemText className="carModal-car-specs" primary={<Text tooltip={data.fuelDetails}>{data.fuelDetails}</Text>} />
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
      </List>
    </div>
  );
}
