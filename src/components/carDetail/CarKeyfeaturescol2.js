import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import { green } from "@material-ui/core/colors";
import { red } from "@material-ui/core/colors";
import SvgIcon from '@material-ui/core/SvgIcon';

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

function OptionalIcon(props) {
return (
<SvgIcon {...props} >
 <path d="M 11.101562 0.554688 C 6.117188 0.921875 1.820312 4.59375 0.695312 9.449219 C 0.308594 11.125 0.367188 13.523438 0.828125 15.222656 C 1.15625 16.417969 1.902344 17.917969 2.714844 19.003906 C 5.976562 23.398438 11.964844 24.851562 16.949219 22.453125 C 18.34375 21.78125 19.289062 21.09375 20.339844 19.972656 C 21.332031 18.933594 21.757812 18.308594 22.394531 16.960938 C 24.117188 13.3125 23.777344 8.90625 21.484375 5.539062 C 19.226562 2.207031 15.164062 0.246094 11.101562 0.554688 Z M 15.886719 6.140625 C 15.921875 6.273438 15.921875 6.425781 15.886719 6.496094 C 15.839844 6.566406 14.679688 6.613281 12.742188 6.613281 L 9.683594 6.613281 L 9.683594 5.90625 L 12.742188 5.90625 C 15.695312 5.90625 15.816406 5.917969 15.886719 6.140625 Z M 11.644531 7.972656 C 11.941406 8.269531 11.964844 8.269531 13.878906 8.269531 C 15.71875 8.269531 15.816406 8.28125 15.886719 8.503906 C 16.050781 9.023438 15.816406 9.09375 13.746094 9.09375 L 11.8125 9.09375 L 11.8125 10.628906 L 13.746094 10.628906 C 15.367188 10.628906 15.707031 10.664062 15.839844 10.820312 C 15.945312 10.949219 15.957031 11.054688 15.863281 11.195312 C 15.769531 11.363281 15.472656 11.410156 14.339844 11.457031 C 13.558594 11.492188 13.003906 11.5625 13.085938 11.609375 C 13.167969 11.667969 13.226562 11.953125 13.226562 12.296875 C 13.226562 12.613281 13.289062 12.875 13.347656 12.875 C 13.417969 12.875 13.464844 12.660156 13.464844 12.402344 C 13.464844 11.824219 13.84375 11.503906 14.316406 11.679688 C 14.644531 11.8125 14.765625 12.011719 14.777344 12.554688 L 14.789062 12.933594 L 14.882812 12.53125 C 14.988281 12.070312 15.164062 11.929688 15.613281 11.929688 C 16.132812 11.929688 16.203125 12.210938 16.15625 14.339844 C 16.121094 16.203125 16.109375 16.277344 15.769531 16.949219 C 14.457031 19.511719 10.582031 19.429688 9.378906 16.832031 L 9.105469 16.238281 L 6.910156 16.121094 L 6.910156 15.414062 L 8.007812 15.378906 L 9.09375 15.34375 L 9.09375 14.953125 C 9.09375 14.738281 9.128906 14.398438 9.164062 14.195312 L 9.234375 13.832031 L 8.078125 13.796875 L 6.910156 13.761719 L 6.910156 13.050781 L 8.292969 13.015625 C 9.320312 12.992188 9.695312 12.945312 9.804688 12.816406 C 10.003906 12.578125 10.238281 12.589844 10.382812 12.839844 C 10.476562 13.003906 10.5 12.851562 10.511719 12.261719 L 10.511719 11.46875 L 8.714844 11.433594 L 6.910156 11.398438 L 6.910156 10.6875 L 8.714844 10.652344 L 10.511719 10.617188 L 10.511719 9.09375 L 8.835938 9.09375 C 7.902344 9.09375 7.074219 9.046875 6.992188 8.988281 C 6.898438 8.941406 6.851562 8.765625 6.875 8.609375 C 6.910156 8.328125 6.910156 8.328125 8.667969 8.292969 C 10.371094 8.269531 10.441406 8.257812 10.699219 7.972656 C 11.042969 7.59375 11.269531 7.605469 11.644531 7.972656 Z M 11.644531 7.972656 "/>
<path  d="M 11.824219 12.1875 L 11.835938 12.933594 L 11.929688 12.332031 C 11.976562 11.988281 12.058594 11.65625 12.09375 11.585938 C 12.140625 11.515625 12.09375 11.457031 12 11.457031 C 11.847656 11.457031 11.8125 11.597656 11.824219 12.1875 Z M 11.824219 12.1875 "/>
<path  d="M 10.417969 14.113281 C 10.417969 14.304688 10.441406 14.375 10.476562 14.257812 C 10.5 14.148438 10.5 13.984375 10.476562 13.902344 C 10.441406 13.832031 10.417969 13.914062 10.417969 14.113281 Z M 10.417969 14.113281 "/>
</SvgIcon>
);
}

export default function CarKeyfeatures(props) {
  const classes = useStyles();
  const aircon = props.data.model_data.aircon;
  const esp = props.data.model_data.ESP;
  const bluetooth = props.data.model_data.bluetooth;
  //const Img = require(`/static/images/avatar/preview.jpg`).ReactComponent;
  
  
  let airconVal;
  if (aircon === "yes") {
    airconVal = <CheckIcon style={{ color: green[500] }} />;
  } else if (aircon === "no") {
    airconVal = <CloseIcon style={{ color: red[500] }} />;
  } else {
    airconVal = <OptionalIcon style={{ fontSize: 30, color: "#1E90FF"}}  />;
  }

  let espVal;
  if (esp === "yes") {
    espVal = <CheckIcon style={{ color: green[500] }} />;
  } else if (esp === "no") {
    espVal = <CloseIcon style={{ color: red[500] }} />;
  } else {
    espVal = <OptionalIcon style={{ fontSize: 30, color: "#1E90FF"}} />;
  }

  let bluetoothVal;
  if (bluetooth === "yes") {
    bluetoothVal = <CheckIcon style={{ color: green[500] }} />;
  } else if (bluetooth === "no") {
    bluetoothVal = <CloseIcon style={{ color: red[500] }} />;
  } else {
    bluetoothVal = <OptionalIcon style={{ fontSize: 30, color: "#1E90FF"}}/>;
  }


  return (
    <div className={classes.root}>
      <List>
        <ListItem>
          <ListItemText primary="Bluetooth" />
          <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="check">
             {bluetoothVal}
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Air Con" />
          <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="check">
              {airconVal}
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="ESP" />
          <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="check">
              {espVal}
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <Divider />
      </List>
    </div>
  );
}
