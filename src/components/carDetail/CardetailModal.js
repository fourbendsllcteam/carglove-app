import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Cardetailgallery from "./Cardetailgallery";
import CarKeyspecificationcol1 from "./CarKeyspecificationcol1";
import CarKeyspecificationcol2 from "./CarKeyspecificationcol2";
import CarKeyfeaturescol1 from "./CarKeyfeaturescol1";
import CarKeyfeaturescol2 from "./CarKeyfeaturescol2";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import "../../App.css";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    top: -5,
    color: "#ffffff",
    right: -5,
    padding: 0,
    position: "absolute",
    background: "#dc3545",
    width: 17,
    height: 17,
    "&:hover": {
      background: "#dc3545",
    },
  },
  title: {
    margin: 0,
    padding: theme.spacing(2),
    position: "relative",
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.title} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon fontSize="small" />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs(props) {
  const { data } = props;
  const [fullWidth] = React.useState(true);
  const [maxWidth] = React.useState("md");
  return (
    <div>
      <Dialog
        onClose={props.handleClose}
        aria-labelledby="customized-dialog-title"
        open={props.openModal}
        fullWidth={fullWidth}
        maxWidth={maxWidth}
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={props.handleClose}
          style={{ textAlign: "center", fontSize: 18, color: "#4f4f4f" }}
        >
          Car Detail
        </DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <Cardetailgallery data={data.modelId} />
            </Grid>

            <Grid item xs={12} sm={12} md={6} lg={6}>
              <List className="car-detailsTop">
                <ListItem>
                  <ListItemText primary="Model ID" />
                  <ListItemSecondaryAction>
                    <ListItemText primary={data.model_data.modelId} />
                  </ListItemSecondaryAction>
                </ListItem>

                <ListItem>
                  <ListItemText primary="Manufacturer" />
                  <ListItemSecondaryAction>
                    <ListItemText primary={data.model_data.manufacturer.name} />
                  </ListItemSecondaryAction>
                </ListItem>

                <ListItem>
                  <ListItemText primary="Model Name" />
                  <ListItemSecondaryAction>
                    <ListItemText primary={data.model_data.modelName} />
                  </ListItemSecondaryAction>
                </ListItem>
              </List>
            </Grid>
          </Grid>

          <Grid container>
            <Typography
              gutterBottom
              style={{
                fontSize: 18,
                color: "#9c1233",
                marginLeft: 16,
                marginBottom: 10,
              }}
            >
              Description
            </Typography>
            <Typography
              gutterBottom
              style={{
                fontSize: 14,
                color: "#4f4f4f",
                marginLeft: 16,
              }}
            >
              {data.model_data.description}
            </Typography>
          </Grid>

          <Grid container>
            <Typography
              style={{
                fontSize: 18,
                marginTop: 20,
                color: "#9c1233",
                paddingLeft: 16,
                display: "block",
                width: "100%",
              }}
            >
              Key Specifications
            </Typography>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <CarKeyspecificationcol1 data={data} />
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <CarKeyspecificationcol2 data={data} />
            </Grid>
          </Grid>

          <Grid container>
            <Typography
              style={{
                fontSize: 18,
                marginTop: 20,
                color: "#9c1233",
                paddingLeft: 16,
                display: "block",
                width: "100%",
              }}
            >
              Key Features
            </Typography>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <CarKeyfeaturescol1 data={data} />
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <CarKeyfeaturescol2 data={data} />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={props.handleClose}
            style={{ color: "#9c1233" }}
          >
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
