import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";
import Home from "./components/home/Home";
import ChangePassword from "./components/user/ChangePassword";
import NotFoundPage from "./NotFoundPage";
import PublicRoute from "./components/Utils/PublicRoute";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <PublicRoute path="/" component={Home} exact/>
        <PublicRoute path="/changePassword" component={ChangePassword} exact/>
        <Route component={NotFoundPage} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
