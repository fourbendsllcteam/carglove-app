import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { store, persistor } from "./store/store";
import "./index.css";
import App from "./App";

ReactDOM.render(
  <React.StrictMode>
    	<Provider store={store}>
      <PersistGate  persistor={persistor}>
        <App />
        </PersistGate>
      </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
