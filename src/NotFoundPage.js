import React from "react";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import Button from "@material-ui/core/Button";
import {createMuiTheme, ThemeProvider, } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#9c1233 !important",
      borderColor: "#9c1233",
      backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    },
  },
});
const useStyles = makeStyles((theme) => ({
  linktxt: {
    fontSize: 14,
    float: "right",
    color: "#ea001e",
    "&:hover": {
      color: "#ea001e",
    },
  },
}));
const NotFoundPage = () => {
  const classes = useStyles();
  const history = useHistory();

  const handleGotoHome = () => {
    history.push("/");
  };
  return (
    <Container component="main" maxWidth="xs">
      <div class="row">
        <div class="col-md-12">
          <div class="error-template">
            <h1>Oops!</h1>
            <h2>404 Not Found</h2>
            <div class="error-details">
              Sorry, an error has occured, Requested page not found!
            </div>
            <div class="error-actions">
              <Grid container>
                <Grid item xs>
                 <ThemeProvider theme={theme}>
                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    style={{
                      marginRight: 10,
                      textTransform: "capitalize",
                      fontSize: 14,
                      marginTop: 10,
                    }}
                    onClick={handleGotoHome}
                  >
                    Go To Home
                  </Button>
                  </ThemeProvider>
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default NotFoundPage;
