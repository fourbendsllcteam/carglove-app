import { v4 as uuidv4 } from "uuid";
import {
  ADD_TO_DEFAULTFILTER,
  FETCH_DEFAULT_FILETRS,
  FETCHED_DEFAULT_FILETRS,
  RECEIVE_ERROR,
  REMOVE_FROM_OPTIONAL,
  REMOVE_FROM_DEFAULT,
  ADD_TO_OPTIONALFILTER,
  FETCH_DEFAULT_PREFERENCES,
  FETCHED_DEFAULT_PREFERENCES,
  ADD_TO_DEFAULT_PREFERENCES,
  REMOVE_FROM_DEFAULT_PREFERENCES,
  TOGGLE_SIGNIN,
  SET_MAX_PRICE,
  SET_PERIOD_MNTHS,
  SET_INTEREST,
  SET_BALLOON_RAND,
  SET_DEPOSIT_RAND,
  SET_BALLOON_PERCENT,
  SET_DEPOSIT_PERCENT,
  SET_MONTHLY_INSTALLMENT,
  CUSTOM_FINANCIAL,
  SET_TRAVELLING_MNTHS,
  SET_INSURANCE,
  SET_DIESEL,
  SET_PETROL,
  UPDATE_PREFERENCES,
  GET_PERCENTILE_VALUES,
  SET_MONTHLY_EXPENDITURE,
  SET_FUEL_CONS,
  SET_ELEC_UNITS,
  SHOW_ANALYSIS_TOAST,
  SET_FINANCIAL,
  TOTAL_RESULT,
  SET_FUEL_SUPPLY
} from "../actions/actionTypes";

const initialState = {
  defaultfilters: {},
  optionalFilters: {},
  defaultPreferences: {},
  optionalPreferences: {},
  defaultPreferenceValue: 50,
  signInStatus: false,
  signInModalOpen: false,
  maxPrice: 100000,
  interest: 5,
  balloonRand: 0,
  depositRand: 150000,
  periodMnths: 48,
  depositPercent: 15,
  balloonPercent: 0,
  monthlyInstallment: 5789000,
  customFinancial: false,
  insurance: 750,
  travellingMnths: 2000,
  diesel: 12.37,
  petrol: 12.69,
  monthlyExpenditure: 10000,
  percentileValues: {},
  fuelCons: 14,
  elecUnits: 2.35,
  analysisToast: false,
  financial:{},
  totalResult: 0,
  fuelSupply:"petrol"
};

const reducer = (state = initialState, action) => {
  console.log("Jothikannan" + JSON.stringify(action));
  switch (action.type) {
    case ADD_TO_DEFAULTFILTER: {
      return {
        ...state,
        defaultfilters: {
          ...state.defaultfilters,
          [action.payload.id]: {
            id: action.payload.id,
            name: action.payload.name,
            description: action.payload.description,
            type: action.payload.type,
            seqNo: action.payload.seqNo,
            isActive: action.payload.isActive,
            visibility: "default",
            field: action.payload.field,
            filterType: action.payload.filterType,
          },
        },
      };
    }

    case FETCH_DEFAULT_FILETRS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        showMoreButtons: false,
      };

    case FETCHED_DEFAULT_FILETRS:
      return {
        ...state,
        defaultfilters: action.payloadDefault,
        optionalFilters: action.payloadOptional,
        showMoreButtons: true,
      };

    case RECEIVE_ERROR:
      return {
        ...state,
        isError: true,
        showMoreButtons: false,
      };

    case REMOVE_FROM_OPTIONAL: {
      console.log(state.optionalFilters);
      const parentKey = Object.keys(state.optionalFilters).filter(
        (key) => state.optionalFilters[key].id === action.payload
      )[0];
      console.log(parentKey);
      const { [parentKey]: value, ...others } = state.optionalFilters;
      return {
        ...state,
        optionalFilters: {
          ...others,
        },
      };
    }

    case REMOVE_FROM_DEFAULT: {
      console.log(state.defaultfilters);
      const parentKey = Object.keys(state.defaultfilters).filter(
        (key) => state.defaultfilters[key].id === action.payload
      )[0];
      console.log(parentKey);
      const { [parentKey]: value, ...others } = state.defaultfilters;
      return {
        ...state,
        defaultfilters: {
          ...others,
        },
      };
    }

    case ADD_TO_OPTIONALFILTER: {
      return {
        ...state,
        optionalFilters: {
          ...state.optionalFilters,
          [action.payload.id]: {
            id: action.payload.id,
            name: action.payload.name,
            description: action.payload.description,
            type: action.payload.type,
            seqNo: action.payload.seqNo,
            isActive: action.payload.isActive,
            visibility: "optional",
            field: action.payload.field,
            filterType: action.payload.filterType,
          },
        },
      };
    }
    case FETCH_DEFAULT_PREFERENCES:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
      };

    case FETCHED_DEFAULT_PREFERENCES:
      return {
        ...state,
        defaultPreferences: action.payload,
      };

    case ADD_TO_DEFAULT_PREFERENCES: {
      const {
        id,
        name,
        label,
        description,
        type,
        seqNo,
        isActive,
        field,
        ratingField,
        defaultValue,
      } = action.payload;
      const updatePreference = Object.keys(state.defaultPreferences).map(
        (key) => {
          if (state.defaultPreferences[key].id === id) {
            state.defaultPreferences[key].name = name;
            state.defaultPreferences[key].label = label;
            state.defaultPreferences[key].description = description;
            state.defaultPreferences[key].type = type;
            state.defaultPreferences[key].seqNo = seqNo;
            state.defaultPreferences[key].isActive = isActive;
            state.defaultPreferences[key].field = field;
            state.defaultPreferences[key].ratingField = ratingField;
            state.defaultPreferences[key].defaultValue = defaultValue;
            state.defaultPreferences[key].visibility = "default";
          }
          return state.defaultPreferences[key];
        }
      );
      return {
        ...state,
        defaultPreferences: updatePreference,
      };
    }
    case REMOVE_FROM_DEFAULT_PREFERENCES: {
      const {
        id,
        name,
        label,
        description,
        type,
        seqNo,
        isActive,
        field,
        ratingField,
        defaultValue,
      } = action.payload;
      const updatePreference = Object.keys(state.defaultPreferences).map(
        (key) => {
          if (state.defaultPreferences[key].id === id) {
            state.defaultPreferences[key].name = name;
            state.defaultPreferences[key].label = label;
            state.defaultPreferences[key].description = description;
            state.defaultPreferences[key].type = type;
            state.defaultPreferences[key].seqNo = seqNo;
            state.defaultPreferences[key].isActive = isActive;
            state.defaultPreferences[key].field = field;
            state.defaultPreferences[key].ratingField = ratingField;
            state.defaultPreferences[key].defaultValue = 50;
            state.defaultPreferences[key].visibility = "optional";
          }
          return state.defaultPreferences[key];
        }
      );
      return {
        ...state,
        defaultPreferences: updatePreference,
      };
    }
    case TOGGLE_SIGNIN: {
      return {
        ...state,
        signInModalOpen: !state.signInModalOpen,
      };
    }

    case SET_MAX_PRICE: {
      return {
        ...state,
        maxPrice: action.payload,
        depositRand: Math.round((action.payload * state.depositPercent) / 100),
        balloonRand: Math.round((action.payload * state.balloonPercent) / 100),
        monthlyInstallment:
          (((action.payload - (action.payload * state.depositPercent) / 100) *
            Math.pow(1 + state.interest / 1200, state.periodMnths) -
            (action.payload * state.balloonPercent) / 100) *
            (state.interest / 1200)) /
          (Math.pow(1 + state.interest / 1200, state.periodMnths) - 1),
      };
    }
    case SET_DEPOSIT_RAND: {
      return {
        ...state,
        depositRand: action.payload,
      };
    }
    case SHOW_ANALYSIS_TOAST: {
      return {
        ...state,
        analysisToast: action.payload,
      };
    }
    case SET_BALLOON_RAND: {
      return {
        ...state,
        balloonRand: action.payload,
      };
    }
    case SET_INTEREST: {
      return {
        ...state,
        interest: action.payload,
        monthlyInstallment:
          (((state.maxPrice - state.depositRand) *
            Math.pow(1 + action.payload / 1200, state.periodMnths) -
            state.balloonRand) *
            (action.payload / 1200)) /
          (Math.pow(1 + action.payload / 1200, state.periodMnths) - 1),
      };
    }
    case SET_PERIOD_MNTHS: {
      return {
        ...state,
        periodMnths: action.payload,
        monthlyInstallment:
          (((state.maxPrice - state.depositRand) *
            Math.pow(1 + state.interest / 1200, action.payload) -
            state.balloonRand) *
            (state.interest / 1200)) /
          (Math.pow(1 + state.interest / 1200, action.payload) - 1),
      };
    }
    case SET_DEPOSIT_PERCENT: {
      return {
        ...state,
        depositPercent: action.payload,
      };
    }
    case SET_BALLOON_PERCENT: {
      return {
        ...state,
        balloonPercent: action.payload,
      };
    }
    case SET_MONTHLY_INSTALLMENT: {
      return {
        ...state,
        monthlyInstallment: action.payload,
      };
    }

    case SET_MONTHLY_EXPENDITURE: {
      var fs;
      if (state.fuelSupply === 'electric') {
        fs = state.elecUnits;
      }
      else if (state.fuelSupply === 'diesel') {
        fs = state.diesel;
      }
      else {
        fs = state.petrol;
      }
      return {
        ...state,
        monthlyExpenditure:
          (state.travellingMnths / 100) * state.fuelCons * fs +
          parseInt(state.insurance) +
          state.monthlyInstallment,
      };
    }
    case SET_FUEL_CONS: {
      return {
        ...state,
        fuelCons: action.payload,
      };
    }
    case SET_ELEC_UNITS: {
      return {
        ...state,
        elecUnits: action.payload,
      };
    }
    case SET_FUEL_SUPPLY:
  {
    return {
      ...state,
      fuelSupply: action.payload,
    };
  }
    case CUSTOM_FINANCIAL: {
      return {
        ...state,
        customFinancial: action.payload,
      };
    }
    case SET_INSURANCE: {
      return {
        ...state,
        insurance: action.payload,
      };
    }
    case SET_TRAVELLING_MNTHS: {
      return {
        ...state,
        travellingMnths: action.payload,
      };
    }

    case SET_DIESEL: {
      return {
        ...state,
        diesel: action.payload,
      };
    }
    case SET_PETROL: {
      return {
        ...state,
        petrol: action.payload,
      };
    }

    case TOTAL_RESULT :{
      return {
        ...state,
        totalResult : action.payload
      }
    }

    case UPDATE_PREFERENCES: {
      console.log(state.defaultPreferences);
      const updatePreference = Object.keys(state.defaultPreferences).map(
        (key) => {
          if (state.defaultPreferences[key].id === action.ID) {
            state.defaultPreferences[key].defaultValue = action.value;
          }
          return state.defaultPreferences[key];
        }
      );
      return {
        ...state,
        defaultPreferences: updatePreference,
      };
    }

    case SET_FINANCIAL: {
      return {
        ...state,
        financial: action.payload,
      };
    }
    case GET_PERCENTILE_VALUES: {
      return {
        ...state,
        percentileValues: action.payload,
      };
    }

    default:
      return state;
  }
};

export default reducer;
