import {
  ADD_TO_DEFAULTFILTER,
  DEFAULT_FILTERS,
  MORE_FILTERS,
  FETCH_DEFAULT_FILETRS,
  FETCHED_DEFAULT_FILETRS,
  RECEIVE_ERROR,
  REMOVE_FROM_OPTIONAL,
  REMOVE_FROM_DEFAULT,
  ADD_TO_OPTIONALFILTER,
  FETCH_DEFAULT_PREFERENCES,
  FETCHED_DEFAULT_PREFERENCES,
  ADD_TO_DEFAULT_PREFERENCES,
  REMOVE_FROM_DEFAULT_PREFERENCES,
  TOGGLE_SIGNIN,
  SET_MAX_PRICE,
  SET_PERIOD_MNTHS,
  SET_INTEREST,
  SET_BALLOON_RAND,
  SET_DEPOSIT_RAND,
  SET_DEPOSIT_PERCENT,
  SET_BALLOON_PERCENT,
  SET_MONTHLY_INSTALLMENT,
  CUSTOM_FINANCIAL,
  SET_INSURANCE,
  SET_TRAVELLING_MNTHS,
  SET_DIESEL,
  SET_PETROL,
  UPDATE_PREFERENCES,
  GET_PERCENTILE_VALUES,
  SET_MONTHLY_EXPENDITURE,
  SET_FUEL_CONS,
  SET_ELEC_UNITS,
  SHOW_ANALYSIS_TOAST,
  SET_FINANCIAL,
  TOTAL_RESULT,
  SET_FUEL_SUPPLY
} from "./actionTypes";

export const adddefaultFilter = (payload) => ({
  type: ADD_TO_DEFAULTFILTER,
  payload,
});

export const defaultFilter = (payload) => ({
  type: DEFAULT_FILTERS,
  payload,
});

export const moreFilter = (payload) => ({
  type: MORE_FILTERS,
  payload,
});

export const fetchDefaultFilters = () => ({
  type: FETCH_DEFAULT_FILETRS,
});

export const fetchedDefaultFilters = (payloadOptional, payloadDefault) => ({
  type: FETCHED_DEFAULT_FILETRS,
  payloadOptional,
  payloadDefault,
});

export const receive_error = () => {
  return {
    type: RECEIVE_ERROR,
  };
};

export const remove_optional_filters = (payload) => {
  return {
    type: REMOVE_FROM_OPTIONAL,
    payload,
  };
};

export const remove_default_filters = (payload) => {
  return {
    type: REMOVE_FROM_DEFAULT,
    payload,
  };
};

export const addmoreFilter = (payload) => ({
  type: ADD_TO_OPTIONALFILTER,
  payload,
});

export const fetchDefaultPreferences = () => ({
  type: FETCH_DEFAULT_PREFERENCES,
});

export const fetchedDefaultPreferences = (payload) => ({
  type: FETCHED_DEFAULT_PREFERENCES,
  payload,
});

export const adddefaultPreferences = (payload) => ({
  type: ADD_TO_DEFAULT_PREFERENCES,
  payload,
});

export const remove_default_preferences = (payload) => {
  return {
    type: REMOVE_FROM_DEFAULT_PREFERENCES,
    payload,
  };
};

export const toggleSignIn = (payload) => {
  return {
    type: TOGGLE_SIGNIN,
    payload,
  };
};

export const showAnalysisToast = (payload) => {
  return {
    type: SHOW_ANALYSIS_TOAST,
    payload,
  };
};

export const setMaxPrice = (payload) => {
  return {
    type: SET_MAX_PRICE,
    payload,
  };
};

export const setDepositRand = (payload) => {
  return {
    type: SET_DEPOSIT_RAND,
    payload,
  };
};

export const setBalloonRand = (payload) => {
  return {
    type: SET_BALLOON_RAND,
    payload,
  };
};

export const setInterest = (payload) => {
  return {
    type: SET_INTEREST,
    payload,
  };
};

export const setPeriodMnths = (payload) => {
  return {
    type: SET_PERIOD_MNTHS,
    payload,
  };
};

export const setBalloonPercent = (payload) => {
  return {
    type: SET_BALLOON_PERCENT,
    payload,
  };
};

export const setDepositPercent = (payload) => {
  return {
    type: SET_DEPOSIT_PERCENT,
    payload,
  };
};

export const setMonthlyInstallment = (payload) => {
  return {
    type: SET_MONTHLY_INSTALLMENT,
    payload,
  };
};

export const isCustomFinancial = (payload) => {
  return {
    type: CUSTOM_FINANCIAL,
    payload,
  };
};

export const setDiesel = (payload) => {
  return {
    type: SET_DIESEL,
    payload,
  };
};

export const setPetrol = (payload) => {
  return {
    type: SET_PETROL,
    payload,
  };
};

export const setTravellingMnths = (payload) => {
  return {
    type: SET_TRAVELLING_MNTHS,
    payload,
  };
};

export const setInsurance = (payload) => {
  return {
    type: SET_INSURANCE,
    payload,
  };
};

export const setFinancial = (payload) => {
  return {
    type: SET_FINANCIAL,
    payload,
  };
};

export const updatePreferences = (value, ID) => ({
  type: UPDATE_PREFERENCES,
  value,
  ID,
});

export const getPercentile = (payload) => {
  return {
    type: GET_PERCENTILE_VALUES,
    payload,
  };
};
export const setFuelSupply = (payload) => {
  return {
    type: SET_FUEL_SUPPLY,
    payload,
  };
};
export const setMonthlyExpenditure = () => {
  return {
    type: SET_MONTHLY_EXPENDITURE,
  };
};

export const setElecUnits = (payload) => {
  return {
    type: SET_ELEC_UNITS,
    payload,
  };
};
export const setFuelCons = (payload) => {
  return {
    type: SET_FUEL_CONS,
    payload,
  };
};

export const setTotal = (payload) => {
  return{
    type : TOTAL_RESULT,
    payload
  }
}

export function fetchScopes() {
  return function (dispatch) {
    dispatch(fetchDefaultFilters());
    return fetch(`${process.env.REACT_APP_API_URL}data/scope`, {
      method: "GET",
    })
      .then(
        (response) => response.json(),
        (error) => console.log("An error occurred.", error)
      )
      .then((json) => {
        let scopes = json.data.scopeResultSet;
        const optionalFilters = scopes.filter(
          (scope) => scope.visibility === "optional"
        );
        const defaultFilters = scopes.filter(
          (scope) => scope.visibility === "default"
        );
        console.log(defaultFilters);
        dispatch(fetchedDefaultFilters(optionalFilters, defaultFilters));
      });
  };
}

export function fetchPreferences() {
  return function (dispatch) {
    dispatch(fetchDefaultPreferences());
    return fetch(`${process.env.REACT_APP_API_URL}data/preference`, {
      method: "GET",
    })
      .then(
        (response) => response.json(),
        (error) => console.log("An error occurred.", error)
      )
      .then((json) => {
        let preferences = json.data.resultSet;
        console.log("pref: " + preferences);
        dispatch(fetchedDefaultPreferences(preferences));
      });
  };
}
